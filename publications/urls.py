from django.conf.urls import patterns, url

urlpatterns = patterns('publications.views',
    url(r'^$', 'publications', name='publications'),
    url(r'^(?P<slug>.*)/$', 'detail', name='detail'),
)