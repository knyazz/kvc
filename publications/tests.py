# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class PublicationsTest(BaseTest):
    def publications_simple_tests(self):
        '''
            test publications functionality
        '''
        # get publications list page
        response = self.test_client.get(reverse('publications:publications'))
        self.assertEqual(response.status_code, 200)
        # get publication page
        kw = dict(slug='ekspoforum-predstavili-v-germanii')
        response = self.test_client.get(reverse('publications:detail', kwargs=kw))
        self.assertEqual(response.status_code, 200)