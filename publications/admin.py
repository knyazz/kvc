#coding:  utf-8
from django.contrib import admin

from base.admin import FlatPageAdmin

from .models import Publication


class PublicationAdmin(FlatPageAdmin):
    def __init__(self, *args, **kwargs):
        super(PublicationAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
                (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ('date',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ('ru_site', 'ru_source', 'ru_flr')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ('en_site', 'en_source', 'en_flr')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ('cn_site', 'cn_source', 'cn_flr')
            }),
        )
admin.site.register(Publication, PublicationAdmin)
