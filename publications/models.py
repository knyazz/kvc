# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from filer.fields.file import FilerFileField

from base.defaults import SITE_HELP_TEXT
from base.models import AbstractPage, LocaleAttrMixin


class Publication(LocaleAttrMixin, AbstractPage):
    u''' Публикации портала '''
    ru_site = models.URLField(_(u'url источника'), help_text=SITE_HELP_TEXT,
                             blank=True)
    ru_source = models.CharField(_(u'Источник'), max_length=255)
    en_site = models.URLField(_(u'url источника'), help_text=SITE_HELP_TEXT,
                             blank=True)
    en_source = models.CharField(_(u'Источник'), max_length=255, blank=True)
    cn_site = models.URLField(_(u'url источника'), help_text=SITE_HELP_TEXT,
                             blank=True)
    cn_source = models.CharField(_(u'Источник'), max_length=255, blank=True)
    date = models.DateField(_(u'Дата публикации'))
    ru_flr = FilerFileField(verbose_name=_(u'Файл (pdf)'), null=True, blank=True,
                            related_name='pubruflr')
    en_flr = FilerFileField(verbose_name=_(u'Файл (pdf)'), null=True, blank=True,
                            related_name='pubenflr')
    cn_flr = FilerFileField(verbose_name=_(u'Файл (pdf)'), null=True, blank=True,
                            related_name='pubcnflr')

    class Meta:
        ordering = '-date', '-created',
        verbose_name= _(u'Сми о нас')
        verbose_name_plural = verbose_name

    def get_absolute_url(self):
        return reverse('publications:detail', kwargs=dict(slug=self.slug))