#coding: utf-8
from django.views.generic.list import ListView

from base.views import DetailPage, CTXMixin

from .models import Publication


class Publicatons(CTXMixin, ListView):
    u''' страница Сми о нас '''
    template_name = 'media.html'
    model = Publication
    paginate_by = 10

    def get_context_data(self, **kwargs):
        ctx = super(Publicatons, self).get_context_data(**kwargs)
        return ctx

    def get_queryset(self):
        qs = super(Publicatons, self).get_queryset(
                                       )#.active_pages(self.request)
        return qs
publications = Publicatons.as_view()

detail = DetailPage.as_view(model=Publication)