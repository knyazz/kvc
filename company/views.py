#coding: utf-8
from django.core.mail import send_mail
from django.http import HttpResponse

from rest_framework import generics

from base.models import SystemTunes, GreenBlock
from base.views import ListPage, SendEmailMixin


from .forms import DPMailSendForm, VacancyOrderForm
from .models import DirectPerson, Vacancy, Department, VacancyOrder


class DPList(ListPage):
    model=DirectPerson
    template_name='direction.html'

    def get_context_data(self, **kwargs):
        ctx = super(DPList, self).get_context_data(**kwargs)
        ctx['mailto_form'] = DPMailSendForm()
        return ctx
direction = DPList.as_view()

class VacancyList(ListPage):
    model=Vacancy
    template_name='vacancies.html'

    def get_context_data(self, **kwargs):
        ctx = super(VacancyList, self).get_context_data(**kwargs)
        ctx['mailto_form'] = VacancyOrderForm()
        ctx['greenblock'] = GreenBlock.objects.filter(page=2).last()
        return ctx
vacancies = VacancyList.as_view()
dep_contacts = ListPage.as_view(   model=Department,
                                template_name='dep_contacts.html')


def send_dpmail(request):
    u'''
        Модальное окно "написать письмо"
    '''
    st = SystemTunes.objects.filter(is_active=True).last()
    form = DPMailSendForm(data=request.POST)
    if form.is_valid():
        data = form.cleaned_data
        msg = u'ФИО: {fio}\nКонтактный телефон: {phone}\nemail: {email}\nСообщение: {message}'.format(**data)
        to = form.cleaned_data['mail_to'].email,
        subject = u'Вам письмо'
        frm = st.server_email if st and st.server_email else 'no-reply@expoforum.ru'
        send_mail(subject, msg, frm, to, fail_silently=True)
        return HttpResponse("OK")
    else:
        return HttpResponse(form.errors)


class VacancyOrderCreate(SendEmailMixin, generics.CreateAPIView):
    u'''
        Модальное окно "отправить резюме"
    '''
    model = VacancyOrder

    def post_save(self, obj, created=False):
        obj.page_title = u'Вакансии'
        self.send_email(obj, email_tpl_type=0, to_attr='vac_to')
vacancy_send = VacancyOrderCreate.as_view()