# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class CompanyTest(BaseTest):
    def company_simple_tests(self):
        '''
            test company functionality
        '''
        # get direction page
        response = self.test_client.get(reverse('company:direction'))
        self.assertEqual(response.status_code, 200)
        # get vacancies page
        response = self.test_client.get(reverse('company:vacancies'))
        self.assertEqual(response.status_code, 200)
        # get dep_contacts page
        response = self.test_client.get(reverse('company:dep_contacts'))
        self.assertEqual(response.status_code, 200)
        # send email for directions
        data = {
                    'fio': 'test',
                    'phone': '111111',
                    'email': 'test@test.com',
                    'message': 'test',
                    'mail_to': 1,
        }
        response = self.test_client.post(reverse('company:send_dpmail'), data)
        self.assertEqual(response.status_code, 200)
        # send vacancy request
        data.pop('mail_to')
        data.update(dict(vacancy=1,))
        response = self.test_client.post(reverse('company:vacancy_send'), data)
        self.assertEqual(response.status_code, 302)