#coding: utf-8
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField
from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField

from base.models import AbstractPage, TitlePage


class Department(TitlePage):
    class Meta:
        verbose_name=_(u'Подразделение')
        verbose_name_plural=_(u'Подразделения')


class PersonContactsAbstractPage(models.Model):
    order = models.PositiveIntegerField(_(u'Сортировка'))
    def __unicode__(self):
        return self.fio()
    ru_fio = models.CharField(_(u'ФИО (рус.)'),
                                max_length=255, blank=True)
    en_fio = models.CharField(_(u'ФИО (англ.)'),
                                max_length=255, blank=True)
    cn_fio = models.CharField(_(u'ФИО (кит.)'),
                                max_length=255, blank=True)
    def fio(self, request=None):
        if request:
            return {
                    'ru': self.ru_fio,
                    'en': self.en_fio,
                    'zh-cn': self.cn_fio,
            }.get(request.LANGUAGE_CODE)
        return self.ru_fio

    ru_post = models.CharField(_(u'Должность (рус.)'),
                                max_length=255, blank=True)
    en_post = models.CharField(_(u'Должность (англ.)'),
                                max_length=255, blank=True)
    cn_post = models.CharField(_(u'Должность (кит.)'),
                                max_length=255, blank=True)

    def post(self, request=None):
        if request:
            return {
                    'ru': self.ru_post,
                    'en': self.en_post,
                    'zh-cn': self.cn_post,
            }.get(request.LANGUAGE_CODE)
        return self.ru_post

    ru_content = RichTextField(_(u'Описание (рус.)'), blank=True)
    en_content = RichTextField(_(u'Описание (англ.)'), blank=True)
    cn_content = RichTextField(_(u'Описание (кит.)'), blank=True)
    class Meta:
        abstract = True

    def content(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_content,
                    'en': self.en_content,
                    'zh-cn': self.cn_content,
            }.get(request.LANGUAGE_CODE)
        return self.ru_content

    phone = models.CharField(_(u'Телефон'), max_length=255, blank=True)
    phone_postfix = models.CharField(_(u'Добавочный телефон'), max_length=255,
                                    blank=True)
    email = models.EmailField(_(u'Email'), blank=True)
    class Meta:
        abstract=True


class DirectPerson(PersonContactsAbstractPage):
    flr_image = FilerImageField(verbose_name=_(u'Фото'),
                                null=True, blank=True)

    @property
    def avatar(self):
        if self.flr_image:
            opts = dict(size=(210,270))
            thumbnail = get_thumbnailer(self.flr_image).get_thumbnail(opts)
            return thumbnail

    class Meta:
        ordering = 'order', 'ru_fio',
        verbose_name = verbose_name_plural =_(u'Руководство')


class DepartmentPerson(PersonContactsAbstractPage):
    department = models.ForeignKey(Department,
                                verbose_name=Department._meta.verbose_name)
    flr_image = FilerImageField(verbose_name=_(u'Фото'),
                                null=True, blank=True)

    @property
    def avatar(self):
        if self.flr_image:
            opts = dict(size=(210,270),crop=True)
            thumbnail = get_thumbnailer(self.flr_image).get_thumbnail(opts)
            return thumbnail

    @property
    def avatar_url(self):
        if self.avatar:
            return self.avatar.url
        return settings.STATIC_URL+'images/SuperE_210x270.png'

    class Meta:
        ordering = 'order', 'ru_fio',
        verbose_name = _(u'Контакт подразделений')
        verbose_name_plural =_(u'Контакты подразделений')


class Vacancy(AbstractPage):
    class Meta:
        verbose_name=_(u'Вакансия')
        verbose_name_plural=_(u'Вакансии')


class VacancyOrder(models.Model):
    vacancy = models.ForeignKey(Vacancy, blank=True, null=True,
                                verbose_name=Vacancy._meta.verbose_name)
    fio = models.CharField(_(u'ФИО'), max_length=1024,)
    phone = models.CharField(_(u'Контактный телефон'), max_length=32)
    email = models.EmailField(_('Email'))
    message = models.CharField(_(u'Сообщение'), max_length=4096,)
    resume = models.FileField(_(u'Резюме'), null=True, blank=True,
                            upload_to='vacancyorder/resume/')
    created = models.DateTimeField(_(u'Дата создания'), auto_now_add=True)
    __unicode__ = lambda self: u'Резюме #{0} от {1}'.format(self.pk,self.fio)
    class Meta:
        verbose_name=u'Резюме вакансии'
        verbose_name_plural = u'Резюме вакансий'

    @property
    def admin_link(self):
        if self.pk:
            return reverse('admin:company_vacancyorder_change',
                            args=(self.pk,),)

    @property
    def absolute_admin_link(self):
        return 'http://'+Site.objects.get_current().domain+self.admin_link