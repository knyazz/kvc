from django.conf.urls import patterns, url

urlpatterns = patterns('company.views',
    url(r'^direction/$', 'direction', name='direction'),
    url(r'^vacancies/$', 'vacancies', name='vacancies'),
    url(r'^dep_contacts/$', 'dep_contacts', name='dep_contacts'),
    url(r'^send_dpmail/$', 'send_dpmail', name='send_dpmail'),
    url(r'^vacancy_send/$', 'vacancy_send', name='vacancy_send'),
)