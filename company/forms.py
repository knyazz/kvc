#coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import DirectPerson, VacancyOrder

opts = dict(required='required',)


class FormMixin(forms.Form):
    fio = forms.CharField(label=_(u'ФИО'), required=True,
                            min_length=1, max_length=1024,)
    phone = forms.CharField(label=_(u'Контактный телефон'), required=False,
                            min_length=1, max_length=32)
    email = forms.EmailField(label=_('Email'), required=False)
    message = forms.CharField(label=_(u'Сообщение'), required=False,
                            max_length=4096,
                            widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        super(FormMixin, self).__init__(*args, **kwargs)
        for f in self.fields.values():
                f.widget.attrs=opts

class DPMailSendForm(FormMixin):
    mail_to = forms.ModelChoiceField(queryset=DirectPerson.objects.all(),
                            required=True, widget=forms.HiddenInput())


class VacancyOrderForm(forms.ModelForm):
    message = forms.CharField(label=_(u'Сообщение'), required=False,
                            max_length=4096,
                            widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        super(VacancyOrderForm, self).__init__(*args, **kwargs)
        for f in self.fields.values():
                f.widget.attrs=opts

    class Meta:
        model = VacancyOrder