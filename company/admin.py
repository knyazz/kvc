#coding: utf-8
from django import forms
from django.contrib import admin
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from suit.admin import SortableModelAdmin

from base.admin import AdminBaseMixin, FlatPageAdmin#, DEFAULT_FIELDSETS
from base.admin import HallVacancyAccrMixin

from .defaults import DEFAULT_VACANCY
from .models import DirectPerson, Vacancy, DepartmentPerson, Department
from .models import VacancyOrder


admin.site.register(Department)


class Vacancyform(forms.ModelForm):
    class Meta:
        model = Vacancy
        exclude_fields='template',

    def __init__(self, *args, **kwargs):
        super(Vacancyform, self).__init__(*args, **kwargs)
        self.fields['ru_content'].initial = DEFAULT_VACANCY

class VacancyAdmin(FlatPageAdmin):
    form = Vacancyform
    def __init__(self, *args, **kwargs):
        super(VacancyAdmin, self).__init__(*args, **kwargs)
        self.fieldsets = self.fieldsets[1:] #delete template in form
admin.site.register(Vacancy, VacancyAdmin)


class DirectPersonAdmin(SortableModelAdmin, AdminBaseMixin):
    sortable = 'order'
    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ('phone', 'phone_postfix', 'email', 'flr_image')
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_fio', 'ru_post', 'ru_content',)
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_fio', 'en_post', 'en_content',)
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_fio', 'cn_post', 'cn_content',)
        }),
    )
    suit_form_tabs = (('general', _(u'Общее'),),)+settings.LANGUAGES
admin.site.register(DirectPerson, DirectPersonAdmin)


class DepartmentPersonAdmin(DirectPersonAdmin):
    def __init__(self, *args, **kwargs):
        super(DepartmentPersonAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
                            (None, {
                                'classes': ('suit-tab suit-tab-general',),
                                'fields': ('department',)
                            }),
                        )
admin.site.register(DepartmentPerson, DepartmentPersonAdmin)


class VacancyOrderAdmin(HallVacancyAccrMixin):
    pass
admin.site.register(VacancyOrder, VacancyOrderAdmin)