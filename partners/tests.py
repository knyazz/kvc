# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class PartnersTest(BaseTest):
    def partners_simple_tests(self):
        '''
            test partners functionality
        '''
        # get partner`s page
        response = self.test_client.get(reverse('partners:partners'))
        self.assertEqual(response.status_code, 200)
        # get partner page
        #kw = dict(pk=3)
        #response = self.test_client.get(reverse('partners:detail', kwargs=kw))
        #self.assertEqual(response.status_code, 200)