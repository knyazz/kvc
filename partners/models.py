# coding: utf-8
from django.conf import settings
#from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField

from base.defaults import SITE_HELP_TEXT
from base.models import SimpleAbstractPage


class PartnerType(models.Model):
    u''' Тематики новостей '''
    ru_title = models.CharField(_(u'Заголовок (рус.)'), max_length=255)
    en_title = models.CharField(_(u'Заголовок (англ.)'), max_length=255)
    cn_title = models.CharField(_(u'Заголовок (кит.)'), max_length=255)

    def __unicode__(self):
        return self.title()

    def title(self, request=None):
        if request:
            return {
                    'ru': self.ru_title,
                    'en': self.en_title,
                    'zh-cn': self.cn_title,
            }.get(request.LANGUAGE_CODE)
        return self.ru_title

    class Meta:
        verbose_name = _(u'Класс партнера')
        verbose_name_plural = _(u'Классы партнеров')


class Partner(SimpleAbstractPage):
    u''' партнер '''
    site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT)
    en_site = models.URLField(_(u'сайт (англ.)'), help_text=SITE_HELP_TEXT,
                            blank=True)
    cn_site = models.URLField(_(u'сайт (кит.)'), help_text=SITE_HELP_TEXT,
                            blank=True)
    partnertype = models.ForeignKey(PartnerType,
                                    verbose_name=PartnerType._meta.verbose_name)
    flr_image = FilerImageField(verbose_name=_(u'Изображение баннера'),
                                related_name='ru_partners',
                                null=True, blank=True)
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True,blank=True,
                                related_name='en_partners')
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True,blank=True,
                                related_name='cn_partners')
    on_main_page = models.BooleanField(_(u'Показывать на главной'), default=True)
    order = models.IntegerField(_(u'Порядок отображения'), default=0)

    def __unicode__(self):
        return self.title() or u'{0} {1}'.format(self._meta.verbose_name,
                                                self.site) 
    def __init__(self, *args, **kwargs):
        super(Partner, self).__init__(*args, **kwargs)
        field = self._meta.get_field_by_name('ru_content')[0]
        field.blank = True

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.flr_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
        return self.flr_image

    def image_url(self, request=None):
        image = self.image(request) or self.flr_image
        if image:
            opts = dict(size=(160,80))
            thumbnail = get_thumbnailer(image).get_thumbnail(opts)
            return thumbnail.url
        return settings.STATIC_URL+'images/partner3.png'

    def cur_site(self, request=None):
        site = self.site
        if request and request.LANGUAGE_CODE:
            site = {
                    'ru': self.site,
                    'en': self.en_site,
                    'zh-cn': self.cn_site,
            }.get(request.LANGUAGE_CODE)
        return site or self.site
    
    class Meta:
        verbose_name = _(u'партнер')
        verbose_name_plural = _(u'партнеры')
        ordering = 'order',

    #def get_absolute_url(self):
        #return reverse('partners:detail', kwargs=dict(pk=self.pk))