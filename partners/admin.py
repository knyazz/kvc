from django.contrib import admin

from suit.admin import SortableModelAdmin

from base.admin import AdminBaseMixin
from base.admin import SimplePageAdmin

from .models import Partner, PartnerType

class PartnerTypeAdmin(AdminBaseMixin):
    prepopulated_fields = {'en_title': ('ru_title',),
                            'cn_title': ('ru_title',),
                        }
admin.site.register(PartnerType, PartnerTypeAdmin)

class PartnerAdmin(SortableModelAdmin, SimplePageAdmin):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'partnertype', 'on_main_page')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ('site',  'flr_image')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ('en_site', 'en_image')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ('cn_site', 'cn_image')
            }),
        )
admin.site.register(Partner, PartnerAdmin)