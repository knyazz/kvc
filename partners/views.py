#coding: utf-8
from django.db.models import Q
from django.views.generic.list import ListView

from base.views import DetailPage, CTXMixin

from .models import Partner, PartnerType


class Partners(CTXMixin, ListView):
    u''' страница новостей'''
    template_name = 'partners.html'
    model = Partner
    paginate_by = 10

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        subjects = PartnerType.objects.all()
        ctx['subjects'] = subjects
        return ctx

    def get_queryset(self):
        qs = super(self.__class__, self).get_queryset(
                                       )#.active_pages(self.request)
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.getlist('partnertype__pk'):
                q&= Q(partnertype__pk__in=body.getlist('partnertype__pk'))
        return qs.filter(q)
partners = Partners.as_view()

detail = DetailPage.as_view(model=Partner)