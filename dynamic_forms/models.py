#coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _

from forms_builder.forms.models import AbstractForm, AbstractFormEntry
from forms_builder.forms.models import AbstractFieldEntry, AbstractField


class FormEntry(AbstractFormEntry):
    form = models.ForeignKey("Form", related_name="entries")


class FieldEntry(AbstractFieldEntry):
    entry = models.ForeignKey("FormEntry", related_name="fields")


class Field(AbstractField):
    """
    Implements automated field ordering.
    """
    form = models.ForeignKey("Form", related_name="fields")
    order = models.IntegerField(_("Order"), null=True, blank=True)

    class Meta(AbstractField.Meta):
        ordering = ("order",)

    def save(self, *args, **kwargs):
        if self.order is None:
            self.order = self.form.fields.count()
        super(Field, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        fields_after = self.form.fields.filter(order__gte=self.order)
        fields_after.update(order=models.F("order") - 1)
        super(Field, self).delete(*args, **kwargs)


class Form(AbstractForm):
    def get_absolute_url(self):
        return reverse("dynamic_forms:form_detail", kwargs= {"slug": self.slug})

    def admin_links(self):
        kw = {"args": (self.id,)}
        links = [
            (_("View form on site"), self.get_absolute_url()),
            (_("Filter entries"), reverse("admin:dynamic_form_entries", **kw)),
            (_("View all entries"), reverse("admin:dynamic_form_entries_show", **kw)),
            (_("Export all entries"), reverse("admin:dynamic_form_entries_export", **kw)),
        ]
        for i, (text, url) in enumerate(links):
            links[i] = "<a href='%s'>%s</a>" % (url, ugettext(text))
        return "<br>".join(links)
    admin_links.allow_tags = True
    admin_links.short_description = ""