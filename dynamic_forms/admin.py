#coding: utf-8
from django.conf.urls import patterns, url
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from forms_builder.forms.admin import FormAdmin
from suit.admin import SortableStackedInline

from .models import Form, FieldEntry, FormEntry, Field


form_admin_fieldsets = (
    (None, {"fields": ("title", ("status", "login_required",),
                                ("publish_date", "expiry_date",),
                        "intro", "button_text", "response"),
            'classes': ('suit-tab suit-tab-general',),
    }),
    (_("Email"), {"fields": ("send_email", "email_from", "email_copies",
                            "email_subject", "email_message"),
                'classes': ('suit-tab suit-tab-email',),
    }),
)
form_admin_formtabs = (('general', _(u'Общее'),), ('email', _(u'Email'),))

class FieldAdmin(SortableStackedInline):
    model = Field
    exclude = ('slug', )
    extra=0
    sortable = 'order'


class DynamicFormAdmin(FormAdmin):
    formentry_model = FormEntry
    fieldentry_model = FieldEntry

    inlines = (FieldAdmin,)
    fieldsets = form_admin_fieldsets
    suit_form_tabs = form_admin_formtabs
    def get_urls(self):
        """
        Add the entries view to urls.
        """
        urls = super(FormAdmin, self).get_urls()
        extra_urls = patterns("",
            url("^(?P<form_id>\d+)/entries/$",
                self.admin_site.admin_view(self.entries_view),
                name="dynamic_form_entries"),
            url("^(?P<form_id>\d+)/entries/show/$",
                self.admin_site.admin_view(self.entries_view),
                {"show": True}, name="dynamic_form_entries_show"),
            url("^(?P<form_id>\d+)/entries/export/$",
                self.admin_site.admin_view(self.entries_view),
                {"export": True}, name="dynamic_form_entries_export"),
            url("^file/(?P<field_entry_id>\d+)/$",
                self.admin_site.admin_view(self.file_view),
                name="dynamic_form_file"),
        )
        return extra_urls + urls
admin.site.register(Form, DynamicFormAdmin)