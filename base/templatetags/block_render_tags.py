#coding: utf-8
from __future__ import unicode_literals
from future.builtins import str

import re

from django import template
from django.template.loader import get_template
from django.utils.translation import get_language

from forms_builder.forms.forms import FormForForm
from dynamic_forms.models import Form


register = template.Library()


@register.simple_tag
def render_locale_title(request, page):
    return page and hasattr(page, 'title') and page.title(request) or ''


@register.simple_tag
def render_locale_content(request, page):
    return page and hasattr(page, 'content') and page.content(request) or ''


@register.simple_tag
def render_locale_field(request, page, field):
    string = page and hasattr(page, field) and getattr(page, field)(request) or ''
    return string.replace('\"','')

@register.simple_tag
def render_locale_url(request, item, url):
    return item and hasattr(item, url) and getattr(item, url)(request) or ''


class LocaleAttrValueNode(template.Node):
    def __init__(self, request, item, attr, var_name):
        self.request = request
        self.item = item
        self.attr = attr
        self.var_name = var_name

    def render(self, context):
        value = self.item and getattr(self.item, 'get_locale_attr'
                                    )(self.attr, self.request) or ''
        context[self.var_name] = value
        return ''


@register.assignment_tag
def get_locale_attr_val(request, item, attr):
    return item and getattr(item, 'get_locale_attr')(attr, request) or ''


@register.filter
def unicode2int(value):
    if value and value.isnumeric():
        return int(value)
    return value


@register.filter
def int2unicode(value):
    return unicode(value)


@register.filter
def strip_lang(path):
    pattern = '^(/%s)/' % get_language()
    match = re.search(pattern, path)
    if match:
        return path[match.end(1):]
    return path


class BuiltFormNode(template.Node):

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def render(self, context):
        request = context["request"]
        user = getattr(request, "user", None)
        post = getattr(request, "POST", None)
        files = getattr(request, "FILES", None)
        if self.name != "form":
            lookup = {
                str(self.name): template.Variable(self.value).resolve(context)
            }
            try:
                form = Form.objects.published(for_user=user).get(**lookup)
            except Form.DoesNotExist:
                form = None
        else:
            form = template.Variable(self.value).resolve(context)
        if not isinstance(form, Form) or (form.login_required and not
                                          user.is_authenticated()):
            return ""
        t = get_template("forms/includes/kvc_built_form.html")
        context["form"] = form
        form_args = (form, context, post or None, files or None)
        context["form_for_form"] = FormForForm(*form_args)
        print context["form_for_form"].fields['telefon']
        print dir(context["form_for_form"].fields['telefon'])
        return t.render(context)


@register.tag
def render_built_dynform(parser, token):
    """
    render_build_form takes one argument in one of the following formats:

    {% render_build_form form_instance %}
    {% render_build_form form=form_instance %}
    {% render_build_form id=form_instance.id %}
    {% render_build_form slug=form_instance.slug %}

    """
    try:
        _, arg = token.split_contents()
        if "=" not in arg:
            arg = "form=" + arg
        name, value = arg.split("=", 1)
        if name not in ("form", "id", "slug"):
            raise ValueError
    except ValueError:
        raise template.TemplateSyntaxError(render_built_dynform.__doc__)
    return BuiltFormNode(name, value)


class EscapeJSNode(template.Node):
    def __init__(self, nodelist):
           self.nodelist = nodelist
    
    def render(self, context):
        from django.template.defaultfilters import escapejs
        return escapejs(self.nodelist.render(context).strip())
        
@register.tag
def escapejs(parser, token):
    """
    Escapes characters for use in JavaScript strings.
    
    Sample usage::
    {% escapejs %}
        <p>
            Text to<br />
            <a href="#">escape</a> here.
        </p>
    {% endescapejs %}
    """
    nodelist = parser.parse(('endescapejs',))
    parser.delete_first_token()
    return EscapeJSNode(nodelist)


@register.simple_tag
def upload_js():
    return """
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<tr class="template-upload fade">
<td>
<span class="preview"></span>
</td>
<td>
<p class="name">{%=file.name%}</p>
{% if (file.error) { %}
<div><span class="label label-important">{%=locale.fileupload.error%}</span> {%=file.error%}</div>
{% } %}
</td>
<td>
<p class="size">{%=o.formatFileSize(file.size)%}</p>
{% if (!o.files.error) { %}
<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
{% } %}
</td>
<td>
{% if (!o.files.error && !i && !o.options.autoUpload) { %}
<button class="btn btn-primary start">
<i class="glyphicon glyphicon-upload"></i>
<span>{%=locale.fileupload.start%}</span>
</button>
{% } %}
{% if (!i) { %}
<button class="btn btn-warning cancel">
<i class="glyphicon glyphicon-ban-circle"></i>
<span>{%=locale.fileupload.cancel%}</span>
</button>
{% } %}
</td>
</tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
<tr class="template-download fade">
<td>
<span class="preview">
{% if (file.thumbnailUrl) { %}
<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
{% } %}
</span>
</td>
<td>
<p class="name">
<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
</p>
{% if (file.error) { %}
<div><span class="label label-important">{%=locale.fileupload.error%}</span> {%=file.error%}</div>
{% } %}
</td>
<td>
<span class="size">{%=o.formatFileSize(file.size)%}</span>
</td>
<td>
<button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
<i class="glyphicon glyphicon-trash"></i>
<span>{%=locale.fileupload.destroy%}</span>
</button>
<input type="checkbox" name="delete" value="1" class="toggle">
</td>
</tr>
{% } %}
</script>
"""