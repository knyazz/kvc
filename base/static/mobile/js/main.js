﻿$(document).ready(function(){
	$('.accordiontitle, .accordionlink').click(function(){
		$(this).siblings().slideToggle();
		$(this).parent().toggleClass('closed');
		});
	$('.layersbtns > li > a').click(function(e){
		e.stopPropagation();
		var indextab = $(this).parent().index();
		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');
		$(this).parent().parent().parent().find('.layer:eq('+indextab+')').addClass('active').siblings().removeClass('active');
		});
	$('#datepicker').datepicker({
        onSelect: function(date) {
            $.get( "filter.php",{ start_date: date });
        },
		showOtherMonths:true
		});
});