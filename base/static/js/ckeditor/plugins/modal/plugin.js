CKEDITOR.plugins.add('modal', {
  icons: 'modal',
  init: function(editor) {
    //контекстное меню
    if (editor.contextMenu) {
      editor.addMenuGroup('modalGroup');

      editor.addMenuItem('modalItem', {
        label: 'Редактировать модальное окно',
        icon: this.path + 'icons/modal.png',
        command: 'modal',
        group: 'modalGroup'
      });

      editor.contextMenu.addListener(function(element) {
        var link = element.getAscendant('a', true);
        if (link && link.getAttribute('data-content')) {
          return {
            modalItem: CKEDITOR.TRISTATE_OFF
          };
        }
      });

    }

    //команда открытия попапа с редактором контента модального окна
    editor.addCommand('modal', new CKEDITOR.dialogCommand('modalDialog', {
      allowedContent: 'a[class]',
    }));

    //кнопка для открытия диалога
    editor.ui.addButton('Modal', {
      label: 'Вставить модальное окно',
      command: 'modal',
      toolbar: 'insert'
    });

    CKEDITOR.dialog.add('modalDialog', this.path + 'dialogs/modal.js');
  }
});
