CKEDITOR.dialog.add('modalDialog', function(editor) {
  var contentEditor;
  var modalDefault = {
    content: '',
    width: '60%',
    height: 'auto'
  };

  return {
    title: 'Редактирование модального окна',
    minWidth: 800,
    minHeight: 200,
    contents: [{
      id: 'tab-basic',
      label: 'Главное',
      elements: [{
        type: 'textarea',
        id: 'modalContent',
        label: 'Контент',
        controlStyle: 'display:none;',
        setup: function(element) {
          contentEditor = CKEDITOR.appendTo(this.domId, editor.config, (element.data(
            'content') || modalDefault.content));
        },
        commit: function(element) {
          element.data('content', contentEditor.getData());
        }
      }]
    }, {
      id: 'tab-settings',
      label: 'Настройки',
      elements: [{
        type: 'text',
        id: 'modalWidth',
        label: 'Ширина модального окна',
        setup: function(element) {
          var width = element.data('width') || modalDefault.width;
          this.setValue(width);
        },
        commit: function(element) {
          element.data('width', this.getValue());
        }
      }, {
        type: 'text',
        id: 'modalHeight',
        label: 'Высота модального окна',
        setup: function(element) {
          var height = element.data('height') || modalDefault.height;
          this.setValue(height);
        },
        commit: function(element) {
          element.data('height', this.getValue());
        }
      }]
    }],
    onShow: function() {
      var selection = editor.getSelection();
      var element = selection.getStartElement();

      if (element) {
        element = element.getAscendant('a', true);
      }

      if (!element || element.getName() != 'a') {
        element = editor.document.createElement('a');
        element.setHtml(selection.getSelectedText());

        this.insertMode = true;
      } else {
        this.insertMode = false;
      }

      element.addClass('kvc-modal');
      this.element = element;

      this.setupContent(this.element);
    },

    onHide: function() {
      contentEditor.destroy();
    },

    onOk: function() {
      var link = this.element;

      this.commitContent(link);

      if (this.insertMode) {
        editor.insertElement(link);
      }
    }
  };
});