﻿
$(document).ready(function() {

  //открытие модального окна созданного в ckeditor
  $('.kvc-modal').click(function(e) {
    var $target = $(e.target);
    var content = $target.data('content') || '';

    var modalConfig = {
      content: content,
      padding: 10,
      autoDimensions: false,
      // autoScale: true,
      scrolling: 'auto',
      width: $target.data('width') || '60%',
      height: $target.data('height') || 'auto',
      type: 'html',
      onComplete: function() {
        var $content = $('#fancybox-content');
        var $contentChildren = $('#fancybox-content').children('div');
        var height = $content.height();
        var height2 = $contentChildren.height();

        if ((height2 - height) > 4) {
          $content.css({
            'overflow-y': 'auto'
          });
          $contentChildren.css({
            width: 'auto'
          });
        }
      },
      onClosed: function() {
        $('#fancybox-content').css({
          'overflow-y': 'hidden'
        });
      }
    };

    $.fancybox(modalConfig);

    $('#fancybox-content').css({
      'color': '#333',
      'overflow-x': 'hidden'
    });

  });

  $('#partners').jcarousel({
    wrap: 'circular'
  });
  //$('#promoslider').jcarousel({
  //	wrap: 'circular'
  //});
  $('#banners').jcarousel({
    wrap: 'circular'
  });
  $('#prevslide').click(function() {
    $('#partners').jcarousel('scroll', '-=1');
    return false;
  });
  $('#nextslide').click(function() {
    $('#partners').jcarousel('scroll', '+=1');
    return false;
  });
  $('#prevbanner').click(function() {
    $('#banners').jcarousel('scroll', '-=1');
    return false;
  });
  $('#nextbanner').click(function() {
    $('#banners').jcarousel('scroll', '+=1');
    return false;
  });

  $('.accordiontitle, .accordionlink').click(function() {
    $(this).siblings().slideToggle();
    $(this).parent().toggleClass('closed');
  });

  $(
    'select.niceselect,.eventsfilter.niceselect input[type!=text],input[type=checkbox]:not(.newsfilter input[type=checkbox])'
  ).fancyfields();
  $('.scroll-pane').jScrollPane({
    showArrows: true,
    autoReinitialise: true
  });
  $('#subscribe, .writeme, .sendresume').click(function(e) {
    $('#overlay').fadeIn();
    e.stopPropagation();
    return false;
  });
  $('#overlay, #closemodal').click(function() {
    $('#overlay').fadeOut();
  });
  $('.modalbox').click(function(e) {
    e.stopPropagation();
  });
  $('.dateui').datepicker();

  //partnerlistsort($('#partners'),$(document).width());
  //bannersResort($('#banners'),$(document).width());

  //animate languages list on hover
  $('.languageselect ul').hover(
    function() {
      $(this).animate({
        height: ($(this).find('li').length - 1) * 27 + 'px'
      }, 400);
    },
    function() {
      $(this).animate({
        height: "0px"
      }, 400);
    });
  //submit language form on click
  $('#languageselect .languageicon').click(function(e) {
    e.stopPropagation();
    $('#languageselect input[name=language]').val($(this).attr('href').substring(1));
    $('#languageselect').submit();
  });
  $('input[type=file]').bind('change', function() {
    $(this).parent().find('.filename').val($(this).val());
  });

  $('.pageslist a').click(function(e) {
    $('.middlecontentblock').ScrollTo({
      offsetTop: 100
    });
  });
});
//$(window).resize(function(){
//	partnerlistsort($('#partners'),$(window).width());
//	bannersResort($('#banners'),$(window).width());
//	});

function changeslide(slideNum, slideNav, slideList) {
  slideNav.find('a').removeClass('active');
  slideNav.find('li').eq(slideNum).find('a').addClass('active');
  slideList.find('li').eq(slideNum).fadeIn();
  slideList.find('li').not(slideList.find('li').eq(slideNum)).fadeOut(0);
};

function nextSlide(slideNav) {
  var nextNum = slideNav.find('.active').parent().index() + 1;
  var maxNum = slideNav.find('li').size();
  if (nextNum >= maxNum) {
    nextNum = 0;
  }
  slideNav.find('a').eq(nextNum).trigger('click');
}

function partnerlistsort(obPartners, screenWidth) {
  if (screenWidth < 1830) {
    var partnerList = '<ul class="partnerslist">';
    $.each($('.partnerbox'), function(i, val) {
      if (i % 2) {
        partnerList += '<a href="' + $('.partnerbox:eq(' + i + ')').attr('href') +
          '" class="partnerbox" target="_blank">' + $('.partnerbox:eq(' + i + ')').html() +
          '</a></li>';
      } else {
        partnerList += '<li class="partnerscell"><a href="' + $('.partnerbox:eq(' + i + ')').attr(
            'href') + '" class="partnerbox" target="_blank">' + $('.partnerbox:eq(' + i + ')')
          .html() + '</a>';
      }
    });
    partnerList += '</ul>'
    obPartners.html(partnerList);
    obPartners.jcarousel('reload', {
      wrap: 'circular'
    });
  } else {
    var partnerList = '<ul class="partnerslist">';
    $.each($('.partnerbox'), function(i, val) {
      if ((i % 5) == 4) {
        partnerList += '<a href="' + $('.partnerbox:eq(' + i + ')').attr('href') +
          '" class="partnerbox" target="_blank">' + $('.partnerbox:eq(' + i + ')').html() +
          '</a></li>';
      } else if ((i % 5) == 0) {
        partnerList += '<li class="partnerscell"><a href="' + $('.partnerbox:eq(' + i + ')').attr(
            'href') + '" class="partnerbox" target="_blank">' + $('.partnerbox:eq(' + i + ')')
          .html() + '</a>';
      } else {
        partnerList += '<a href="' + $('.partnerbox:eq(' + i + ')').attr('href') +
          '" class="partnerbox" target="_blank">' + $('.partnerbox:eq(' + i + ')').html() +
          '</a>';
      }
    });
    partnerList += '</ul>'
    obPartners.html(partnerList);
    obPartners.jcarousel('reload', {
      wrap: 'circular'
    });
  }
}

function bannersResort(obBanners, screenWidth) {
  if (screenWidth < 1830) {
    var bannersList = '<ul class="bannerslist">';
    $.each($('.infobanner'), function(i, val) {
      bannersList += '<li>' + $('.infobanner:eq(' + i + ')').parent().parent().html() +
        '</li>';
    });
    bannersList += '</ul>';
    obBanners.html(bannersList);
    obBanners.jcarousel('reload', {
      wrap: 'circular'
    });
    $('#noslidebanner').hide().html('');
  } else {
    var bannersList = '<ul class="bannerslist">';
    $.each($('.infobanner'), function(i, val) {
      if (i) {
        bannersList += '<li>' + $('.infobanner:eq(' + i + ')').parent().parent().html() +
          '</li>';
      }
    });
    bannersList += '</ul>';
    $('#noslidebanner').html($('.infobanner:eq(0)').parent().parent().html()).fadeIn();

    obBanners.html(bannersList);
    obBanners.jcarousel('reload', {
      wrap: 'circular'
    });
  }
}

function set_paginator_pages(data, paginator) {
  var href = data.request_params
  var cur_page = 1;
  if (data.current_page) {
    var cur_page = parseInt(data.current_page[0]);
  }
  $.each(data.viewable_pages, function(index, element) {
    if (element == cur_page) {
      paginator.append('<li><a class=\"currentPage\" href=\"?page=' + element + '&' + href +
        '\">' + element + '</a></li>');
    } else {
      paginator.append('<li><a href=\"?page=' + element + '&' + href + '\">' + element +
        '</a></li>');
    }
  });
}