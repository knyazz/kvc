$(function() {
  available_urls = [
      '/',
      '#',
      '/calendar/',
      '/company/dep_contacts/',
      '/company/direction/',
      '/company/vacancies/',
      '/current/',
      '/filestorage/documents/',
      '/filestorage/mediabank/',
      '/howtopage/',
      '/news/',
      '/organizers/halls/',
      '/organizers/ordering/hall/',
      '/partners/',
      '/plans/show',
      '/press/ordering/accreditation/',
      '/publications/',
    ],
    $('#id_url').autocomplete({
      source: available_urls,
      minLength: 0,
    });
});

$(function() {
  //задача 1045:
  //при включения языковой вкладки нерусской (английской или китайской)
  //и заполнени одного из полей, всем остальным полям выставить атрибут required
  function mustCheck(lang) {
    var mustCheck = false;

    $('[id^="id_' + lang + '"]').each(function() {
      var $el = $(this);
      var elementType = $el.prop('tagName').toLowerCase();

      if (elementType === 'select' && $el.find('option:selected').text() !== 'неактивно') {
        mustCheck = true;
      }
    });

    return mustCheck;
  }

  function checkRequired(lang) {
    var required = false;
    var check = mustCheck(lang);

    $('[id^="id_' + lang + '"]').each(function() {
      var $el = $(this);
      var id = $el.attr('id').replace("id_" + lang, "id_ru");

      if (!!$('#' + id).prop('required') && $el.val()) {
        required = true;
      }
    });

    $('[id^="id_' + lang + '"]').each(function() {
      var $el = $(this);
      var id = $el.attr('id').replace("id_" + lang, "id_ru");
      // console.log('$el', id, $('#' + id).prop('required'));

      $(this).prop('required', (required && check && !!$('#' + id).prop('required')));

    });

  }

  //помечаем вкладку с невалидными полями
  function validateTab(lang) {
    var invalid = false;
    var check = mustCheck(lang);

    if (check) {
      $('[id^="id_' + lang + '"]').each(function() {
        var $el = $(this);

        if ($el.get(0) && $el.get(0).checkValidity) {
          var valid = $el.get(0).checkValidity();
          if (!valid) {
            invalid = true;
          }
        }

      });
    }


    var $tabLink = $('#suit_form_tabs').find('[href=#' + lang + '],[href=#seo' + lang + ']');
    if (invalid) {
      $tabLink.addClass('error');
    } else {
      $tabLink.removeClass('error');
    }
  }


  checkRequired('en');
  validateTab('en');

  // checkRequired('cn');
  // validateTab('cn');

  validateTab('ru');

  $('[id^="id_en"]').on('keyup change', function() {
    checkRequired('en');
    validateTab('en');
  });
  // $('[id^="id_cn"]').on('keyup change', function() {
  //   checkRequired('cn');
  //   validateTab('cn');
  // });
  $('[id^="id_ru"]').on('keyup', function() {
    validateTab('ru');
  });

  $('.submit-row').click(function(e) {
    validateTab('en');
    // validateTab('cn');
    validateTab('ru');
  });

});


$(function() {
  $('#id_fp').on('change', function() {
    var flatPageId = $(this).val();
    if (flatPageId) {

      $.ajax({
        url: '/current/get_flatpage_url/?pk=' + flatPageId,
        success: function(data) {
          $('#id_url').val(data);
        }
      });

      $.ajax({
        url: '/current/check_fp_menuitem_exists/',
        data: {
          pk: flatPageId,
          nocache: (new Date()).getTime()
        },
        success: function(data) {
          if (data && data.exist) {
            alert('Страница принадлежит другому пункту меню');
          }
        }
      });

    }
  });
  $('#id_form').on('change', function() {
    if ($(this).val()) {
      $.ajax({
        url: '/current/get_form_url/?pk=' + $(this).val(),
        success: function(data) {
          $('#id_url').val(data);
        }
      });
    }
  });
});

$(function() {
  function set_image() {
    $('div.control-group.form-row.field-image').show();
    $('div.control-group.form-row.field-video').hide();
    $('div.control-group.form-row.field-fl').hide();
    $('input[name=fl]').val("");
    $('input[name=fl]').attr("required", false);
    $('input[name=video]').val("");
    $('input[name=video]').attr("required", false);
    $('input[name=image]').attr("required", "required");
  }
  set_image();
  $("#id_fl_type").on("change", function() {
    if ($("#id_fl_type").val() == 0) {
      set_image();
    } else if ($("#id_fl_type").val() == 1) {
      $('div.control-group.form-row.field-image').hide();
      $('div.control-group.form-row.field-video').show();
      $('div.control-group.form-row.field-fl').hide();
      $('input[name=fl]').val("");
      $('input[name=fl]').attr("required", false);
      $('input[name=image]').val("");
      $('input[name=image]').attr("required", false);
      $('input[name=video]').attr("required", "required");
    } else if ($("#id_fl_type").val() == 2) {
      $('div.control-group.form-row.field-image').hide();
      $('div.control-group.form-row.field-video').hide();
      $('div.control-group.form-row.field-fl').show();
      $('input[name=video]').val("");
      $('input[name=video]').attr("required", false);
      $('input[name=image]').val("");
      $('input[name=image]').attr("required", false);
      $('input[name=fl]').attr("required", "required");
    }
  });
});

$(function() {
  var fupl = $('.file-upload');
  fupl.each(function(index, value) {
    var fl = value.getElementsByTagName('a');
    if (fl && fl[0].getAttribute('href')) {
      var inputs = value.getElementsByTagName('input');
      if (inputs && inputs[0].attributes.required) {
        inputs[0].attributes.removeNamedItem('required');
      }
    }
  });
});

$(function() {
  if (typeof CKEDITOR !== 'undefined') {

    //две следующие строчки для регистрации в ckeditor плагина создания модальных окон
    CKEDITOR.plugins.addExternal('modal', '/static/js/ckeditor/plugins/modal/', 'plugin.js');
    CKEDITOR.config.extraPlugins = 'modal';

    if ($('#id_template option:selected').val() == 'howto.html') {
      CKEDITOR.config.contentsCss = '/static/css/style.css';
    }
    $('div.cke_contents').attr('style', 'height: 400px;');

    CKEDITOR.on('dialogDefinition', function(ev) {
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;
      dialogDefinition.minHeight = 100;
      dialogDefinition.minWidth = 400;
      var editor = ev.editor;

      dialogDefinition.removeContents('Link');
      dialogDefinition.removeContents('advanced');

      if (dialogName == 'image') {
        var infoTab = dialogDefinition.getContents('info');

        dialogDefinition.onOk = function(e) {
          var imageSrcUrl = e.sender.originalElement.$.src;
          var width = e.sender.originalElement.$.width;
          var height = e.sender.originalElement.$.height;
          var txtHeight = dialogDefinition.dialog.getValueOf("info", "txtHeight");
          var txtWidth = dialogDefinition.dialog.getValueOf("info", "txtWidth");
          if (txtHeight) {
            height = txtHeight;
          };
          if (txtWidth) {
            width = txtWidth;
          };
          var imgHtml = CKEDITOR.dom.element.createFromHtml(
            '<a class=\"fancybox\" href=\"' + imageSrcUrl + '\"><img src=' +
            imageSrcUrl + ' width=\"' + width + '\" height=\"' + height +
            '\" alt=\"\" /></a>');
          editor.insertElement(imgHtml);
        };

        infoTab.remove('txtHSpace');
        infoTab.remove('txtVSpace');
        infoTab.remove('txtBorder');
        //infoTab.remove('txtHeight');
        //infoTab.remove('txtWidth');
        infoTab.remove('cmbAlign');
        infoTab.remove('ratioLock');

        infoTab.get('htmlPreview').style = 'display: none;'
      }
    });
  }
});

$(function() {
  $('#id_event').on('change', function() {
    $.ajax({
      url: '/calendar/api/event_titles/?id=' + $(this).val(),
      success: function(data) {
        if (data) {
          $('#id_subjects').val(data[0].subjects);
          $('#id_expotype').val(data[0].expotype);
        }
      }
    });
  });
});

$(document).ready(function() {
  $('div.cke_contents').attr('style', 'height: 400px;');
});
