# coding: utf-8
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField
from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from .choices import PUBLISH_CHOICES, TEMPLATES, SOCIAL_NET_TYPE
from .choices import SITE_BLOCK_TYPES, STATIC_SITE, EMAIL_TEMPLATES
from .defaults import SITE_HELP_TEXT
from .managers import DefaultManager, SystemtunesManager, SliderManager


class UnicodeNameMixin(object):
    def __unicode__(self):
        return u'{0} {1} {2}'.format(self._meta.verbose_name,
                                    _(u'вариант'), self.pk)


class LocaleAttrMixin(object):
    def get_locale_attr(self, attr, request=None):
        cd = 'ru'
        if request and request.LANGUAGE_CODE:
            cd = request.LANGUAGE_CODE
        _attr = hasattr(self, cd+'_'+attr) and getattr(self, cd+'_'+attr)
        return _attr or hasattr(self, attr) and getattr(self, attr)


class TitlePage(models.Model):
    ru_title = models.CharField(_(u'Заголовок (рус.)'),
                                max_length=255,)# blank=True)
    en_title = models.CharField(_(u'Заголовок (англ.)'),
                                max_length=255, blank=True)
    cn_title = models.CharField(_(u'Заголовок (кит.)'),
                                max_length=255, blank=True)
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title() or unicode(self.pk)

    def title(self, request=None):
        if request:
            return {
                    'ru': self.ru_title,
                    'en': self.en_title,
                    'zh-cn': self.cn_title,
            }.get(request.LANGUAGE_CODE)#.capitalize()
        return self.ru_title#.capitalize()


class SystemTunes(TitlePage):
    objects = SystemtunesManager()
    phone = models.CharField(_(u'Телефон'), max_length=255)
    is_active = models.BooleanField(_(u'Активный'), default=True)
    sliderspeed = models.PositiveSmallIntegerField(_(u'Скорость слайдера, сек'),
        default=10)

    email = models.EmailField(_('email'), help_text=_(u'Отображается на сайте'))
    server_email = models.EmailField(_(u'email сервера'), blank=True)
    to_email = models.EmailField(_(u'Email для корреспонденции'), blank=True)
    accr_email = models.EmailField(_(u'Email для аккредитации'), blank=True)
    vac_email = models.EmailField(_(u'Email для вакансий'), blank=True)
    hall_email = models.EmailField(_(u'Email для бронирования зала'), blank=True)

    google_analytics = models.TextField(_(u'Код Google-аналитики'), blank=True)
    yandex_metrics = models.TextField(_(u'Код Yandex-метрики'), blank=True)

    ru_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    ru_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    en_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    en_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    cn_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    cn_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    class Meta:
        verbose_name= _(u'Системные настройки')
        verbose_name_plural = verbose_name

    def keywords(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_keywords,
                    'en': self.en_keywords,
                    'zh-cn': self.cn_keywords,
            }.get(request.LANGUAGE_CODE)
        return self.ru_keywords

    def description(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_description,
                    'en': self.en_description,
                    'zh-cn': self.cn_description,
            }.get(request.LANGUAGE_CODE)
        return self.ru_description

    sliderspeedms = property(lambda self: self.sliderspeed*1000)

    default_to = property(lambda self: self.to_email or self.email)
    hall_to = property(lambda self: self.hall_email or self.default_to)
    accr_to = property(lambda self: self.accr_email or self.default_to)

    @property
    def vac_to(self):
        return self.vac_email or self.default_to or 'g.kiseleva@expoforum.ru'

    @property
    def from_email(self):
        return self.server_email or 'no-reply@expoforum.ru'


class KVCplan(TitlePage):
    ru_image = FilerImageField(verbose_name=_(u'Изображение (рус.)'),
                                related_name='ru_base_plans')
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True,blank=True,
                                related_name='en_base_plans')
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True,blank=True,
                                related_name='cn_base_plans')
    is_active = models.BooleanField(_(u'Активный'), default=True)

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
        return self.ru_image

    def cur_image_url(self, request=None):
        if self.image(request):
            return self.image(request).url

    class Meta:
        verbose_name= _(u'План КВЦ')
        verbose_name_plural = _(u'Планы КВЦ')


class SimpleAbstractPage(TitlePage):
    template = models.CharField(_(u'Шаблон'),max_length=255, choices=TEMPLATES,
                                default='object_detail.html')
    ru_content = RichTextField(_(u'Содержание (рус.)'), blank=True,
                                default=u'''<p>Описание ещё не добавлено</p>''')
    ru_status = models.PositiveSmallIntegerField(_(u'Статус публикации (рус.)'),
        default=2, choices=PUBLISH_CHOICES)
    en_content = RichTextField(_(u'Содержание (англ.)'), blank=True,
                                default=u'''<p>No description</p>''')
    en_status = models.PositiveSmallIntegerField(_(u'Статус публикации (англ.)'),
        default=2, choices=PUBLISH_CHOICES)
    cn_content = RichTextField(_(u'Содержание (кит.)'), blank=True,
                                default=u'''<p>发展中</p>''')
    cn_status = models.PositiveSmallIntegerField(_(u'Статус публикации (кит.)'),
        default=2, choices=PUBLISH_CHOICES)

    class Meta:
        abstract = True

    def content(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_content,
                    'en': self.en_content,
                    'zh-cn': self.cn_content,
            }.get(request.LANGUAGE_CODE)
        return self.ru_content

    def publish_status(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_status,
                    'en': self.en_status,
                    'zh-cn': self.cn_status,
            }.get(request.LANGUAGE_CODE)
        return self.ru_status


class AbstractPage(SimpleAbstractPage):
    objects = DefaultManager()
    created = models.DateTimeField(_(u'Дата создания'))
    slug = models.SlugField(_(u'Путь'), max_length=255, unique=True,
                help_text=_(u'Заполнить заголовок на русском языке')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    show_date = models.BooleanField(_(u'Показывать дату создания страницы'),
                                    default=True)

    class Meta:
        ordering = ('-created',)
        abstract = True

    def save(self, *args, **kwargs):
        if self.pk is None and not self.created:
            self.created = timezone.now()
        super(AbstractPage, self).save(*args,**kwargs)


class FlatPage(AbstractPage):
    u''' простая страница '''
    url_path = models.CharField(_(u'Принудительный URL'),
                            max_length=2048, blank=True)
    ru_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    ru_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    en_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    en_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    cn_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    cn_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)

    def save(self, *args, **kwargs):
        if self.url_path:
            self.url_path = self.formated_url()
        super(FlatPage, self).save(*args, **kwargs)

    def formated_url(self):
        if self.url_path:
            if not self.url_path.startswith('/'): self.url_path = '/' + self.url_path
            if not self.url_path.endswith('/'): self.url_path += '/'
            return self.url_path

    def fp_keywords(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_keywords,
                    'en': self.en_keywords,
                    'zh-cn': self.cn_keywords,
            }.get(request.LANGUAGE_CODE)
        return self.ru_keywords

    def fp_description(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_description,
                    'en': self.en_description,
                    'zh-cn': self.cn_description,
            }.get(request.LANGUAGE_CODE)
        return self.ru_description

    class Meta:
        verbose_name= _(u'простая страница')
        verbose_name_plural = _(u'простые страницы')

    def get_absolute_url(self):
        if self.url_path:
            return self.url_path
        return reverse('base:flat_page', kwargs=dict(slug=self.slug))

    def get_seo(self, request=None):
        return self.title(request) or self.keywords(request) or self.description(request)


class BaseInfo(models.Model):
    phone = models.CharField(_(u'Телефон'), max_length=255)
    email = models.EmailField(_('email'))
    is_active = models.BooleanField(_(u'Актуально'), default=True)

    def __unicode__(self):
        return u'{0} {1} {2}'.format(self._meta.verbose_name,
                                    self.phone, self.email) 

    class Meta:
        verbose_name= _(u'Контакная информация')
        verbose_name_plural = verbose_name


class SocialNetwork(models.Model):
    site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT)
    net_type = models.CharField(_(u'Тип соцсети'), max_length=32,
                                choices=SOCIAL_NET_TYPE)

    def __unicode__(self):
        return u'{0} {1} {2}'.format(self._meta.verbose_name,
                                    self.net_type, self.site) 

    class Meta:
        verbose_name= _(u'Соцсеть')
        verbose_name_plural = _(u'Соцсети')


class SiteBlock(models.Model):
    block_type = models.PositiveSmallIntegerField(_(u'Тип блока'),
                                default=0, unique=True,
                                choices=SITE_BLOCK_TYPES)
    info = models.ForeignKey(BaseInfo, null=True, blank=True,
                            verbose_name=_(u'Контакная информация'))
    fp = models.ManyToManyField(FlatPage, verbose_name=_(u'простые страницы'), 
                                null=True, blank=True)
    net = models.ManyToManyField(SocialNetwork, verbose_name=_(u'Соцсети'),
                                null=True, blank=True,)

    def __unicode__(self):
        return u'{0} {1} {2}'.format(self._meta.verbose_name,
                                    _(u'тип'), self.get_block_type_display()) 
    class Meta:
        verbose_name= _(u'Блок сайта')
        verbose_name_plural = _(u'Блоки сайта')


class MenuItem(MPTTModel):
    parent = TreeForeignKey('self', null=True, blank=True,
                                verbose_name=_(u'Меню-предок'),
                                related_name='children')
    order = models.PositiveIntegerField(_(u'Сортировка'))
    fp = models.ForeignKey(FlatPage, verbose_name=FlatPage._meta.verbose_name, 
                            null=True, blank=True)
    form = models.ForeignKey('dynamic_forms.Form', verbose_name=_(u'Форма'), 
                            null=True, blank=True)
    url = models.CharField(_(u'URL страницы'), max_length=2048,
                            help_text=_(u'Например: /news/'))
    is_clickable = models.BooleanField(_(u'Кликабельно'),default=True,
                            help_text=_(u'Только для верхнего меню'),)
    icon = FilerImageField(verbose_name=_(u'Пиктограмма'), blank=True,null=True)
    ru_title = models.CharField(_(u'Заголовок (рус.)'),
                                max_length=255,)# blank=True)
    en_title = models.CharField(_(u'Заголовок (англ.)'),
                                max_length=255, blank=True)
    cn_title = models.CharField(_(u'Заголовок (кит.)'),
                                max_length=255, blank=True)
    ru_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    ru_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    en_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    en_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)
    cn_keywords = models.CharField(_(u'SEO keywords'),
                                max_length=2048, blank=True)
    cn_description = models.CharField(_(u'SEO description'),
                                max_length=2048, blank=True)

    def formated_url(self):
        if self.url:
            if not self.url.startswith('/'): self.url = '/' + self.url
            if not self.url.endswith('/'): self.url += '/'
            return self.url

    def keywords(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_keywords,
                    'en': self.en_keywords,
                    'zh-cn': self.cn_keywords,
            }.get(request.LANGUAGE_CODE)
        return self.ru_keywords

    def description(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_description,
                    'en': self.en_description,
                    'zh-cn': self.cn_description,
            }.get(request.LANGUAGE_CODE)
        return self.ru_description

    def __unicode__(self):
        return self.title()

    @property
    def icon_url(self):
        if self.icon:
            opts = dict(size=(60,50))
            thumbnail = get_thumbnailer(self.icon).get_thumbnail(opts)
            return thumbnail.url

    def title(self, request=None):
        if request:
            return {
                    'ru': self.ru_title,
                    'en': self.en_title,
                    'zh-cn': self.cn_title,
            }.get(request.LANGUAGE_CODE).capitalize()#.upper()
        return self.ru_title.capitalize()#.upper()

    def get_seo(self, request=None):
        return self.title(request) or self.keywords(request) or self.description(request)

    class Meta:
        verbose_name= _(u'Элемент меню')
        verbose_name_plural = _(u'Элементы меню')
    class MPTTMeta:
        order_insertion_by = ('order',)

    def save(self, *args, **kwargs):
        if self.url:
            self.url = self.formated_url()
        super(MenuItem, self).save(*args, **kwargs)
        MenuItem.objects.rebuild()

    @property
    def admin_link(self):
        if self.pk:
            return reverse('admin:base_menuitem_change', args=(self.pk,),)


class Menu(models.Model):
    siteblock = models.ForeignKey(SiteBlock, related_name='menu',
                                verbose_name=SiteBlock._meta.verbose_name)
    menuitem = models.ForeignKey(MenuItem, related_name='menu',
                                verbose_name=MenuItem._meta.verbose_name)
    order = models.IntegerField(_(u'Порядок отображения'), default=0)

    class Meta:
        verbose_name= _(u'Элемент меню')
        verbose_name_plural = _(u'Меню')
        ordering = 'order', 'siteblock__block_type', 'menuitem__order'
        unique_together = 'siteblock', 'menuitem'


class SliderItemAbstract(AbstractPage):
    site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT)
    is_active = models.BooleanField(_(u'Показать на главной'), default=True)
    order = models.IntegerField(_(u'Порядок отображения'), default=0)

    class Meta:
        abstract = True
        verbose_name= _(u'Элемент слайдера')
        verbose_name_plural = _(u'Элементы слайдера')    

class SliderItem(SliderItemAbstract):
    objects = SliderManager()
    flr_image = FilerImageField(verbose_name=_(u'Изображение'),
                                related_name='ru_slideritem')
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True,blank=True,
                                related_name='en_slideritem')
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True,blank=True,
                                related_name='cn_slideritem')
    def big_image(self, request=None):
        if self.image(request):
            opts = dict(size=(902,596))
            thumbnail = get_thumbnailer(self.image(request)).get_thumbnail(opts)
            return thumbnail.url

    def small_image(self, request=None):
        if self.image(request):
            opts = dict(size=(594,596))
            thumbnail = get_thumbnailer(self.image(request)).get_thumbnail(opts)
            return thumbnail.url

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.flr_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
        return self.flr_image

    def image_url(self, request=None):
        return self.image(request) and self.image(request).url

    class Meta:
        ordering = 'order',
        verbose_name= _(u'Элемент слайдера')
        verbose_name_plural = _(u'Элементы слайдера') 


class ExpoType(TitlePage):
    class Meta:
        verbose_name=_(u'Тип выставки')
        verbose_name_plural=_(u'Типы выставки')

    @property
    def tag(self):
        return  {
            u'ярмарка': 'blue',
            u'выставка': 'orange',
            u'заседание': 'green',
        }.get(self.title().lower(), 'green')


class Subject(TitlePage):
    order = models.PositiveIntegerField(_(u'Сортировка'))
    class Meta:
        verbose_name=_(u'Тематика')
        verbose_name_plural=_(u'Тематики')
        ordering = 'order',


class GreenBlock(TitlePage):
    page = models.PositiveSmallIntegerField(_(u'Страница сайта'),
                                default=0, unique=True,
                                choices=STATIC_SITE)
    class Meta:
        verbose_name=_(u'''"Зеленый" блок статичных страниц''')
        verbose_name_plural=_(u'''"Зеленые" блоки статичных страниц''')


class EmailTemplate(models.Model):
    subject = models.CharField(_(u'Заголовок'), max_length=255)
    text = models.TextField(_(u'Текст сообщения'))
    tpl_type = models.PositiveSmallIntegerField(
                            _(u'Тип шаблона'),
                            unique=True,
                            choices=EMAIL_TEMPLATES)
    __unicode__ = lambda self: self.get_tpl_type_display()
    class Meta:
        verbose_name=_(u'Шаблон email сообщений')
        verbose_name_plural=_(u'Шаблоны email сообщений')