#coding: utf-8
from django import forms
from django.utils.encoding import smart_text

from django_select2.fields import ModelChoiceFieldMixin
from django_select2.widgets import Select2MultipleWidget


class LocaleModelChoiceFieldMixin(ModelChoiceFieldMixin):
    def label_from_instance(self, obj):
        return smart_text(obj.title(self.request))


class LocaleModelChoiceField(LocaleModelChoiceFieldMixin,
                            forms.ModelChoiceField):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.get('request')
        super(LocaleModelChoiceField, self).__init__(*args, **kwargs)


class LocaleModelSelect2MultipleField(  LocaleModelChoiceFieldMixin, 
                                        forms.ModelMultipleChoiceField):
    queryset = property(LocaleModelChoiceFieldMixin._get_queryset, 
                        forms.ModelMultipleChoiceField._set_queryset)
    widget = Select2MultipleWidget

    def __init__(self, *args, **kwargs):
        self.request = kwargs.get('request')
        super(LocaleModelSelect2MultipleField, self).__init__(*args, **kwargs)
