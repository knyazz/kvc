#coding: utf-8
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

from .utils import path_modificate




class SidebarMenu(object):
    def process_view(self, request, view_func, view_args, view_kwargs):
        path = path_modificate(request)
        if len(path) < 2 and request.flavour == 'mobile':
            return redirect(reverse('calendar:events'))