#coding: utf-8
from django.http import QueryDict

from rest_framework import pagination
from rest_framework import serializers


PAGINATOR_SIZE = 4


class CustomPaginationSerializer(pagination.BasePaginationSerializer):
    next = pagination.NextPageField(source='*')
    previous = pagination.PreviousPageField(source='*')
    pages = serializers.Field(source='paginator.page_range')

    def to_native(self, obj):
        data = super(CustomPaginationSerializer, self).to_native(obj)
        request = self.context.get('request')
        data['current'] = request.get_full_path()
        qp = QueryDict( self.context.get('request').QUERY_PARAMS.urlencode(),
                        mutable=True)
        if qp.get('page'): 
            data['current_page'] = qp.pop('page')
        data['viewable_pages'] = self.viewable_pages(data)
        data['request_params'] = qp.urlencode()
        return data

    def viewable_pages(self, data):
        if data.get('pages'):
            pages = data.get('pages')
            #pages = range(1,20)
            if data.get('current_page'):
                cp = int(data.get('current_page')[0])
                fp = pages[0] #first page
                lp = pages[-1] #last page
                min_page = max(fp, cp-PAGINATOR_SIZE)
                max_page = min(lp, cp+PAGINATOR_SIZE)
                res=[fp,]
                for i in pages[min_page:max_page]:
                    res.append(i)
                if max_page != lp:
                    res.append(lp)
                return res
            else:
                return pages[:PAGINATOR_SIZE]
