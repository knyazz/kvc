#coding: utf-8
import json
from rest_framework import serializers
 

class JSONField(serializers.WritableField):
    def to_native(self, obj):
        #print json.dumps(obj)
        return obj
    def from_native(self, value):
        try:
            return json.loads(value)
        except (TypeError, ValueError):
            raise serializers.ValidationError(
                "Could not load json <{}>".format(value)
            )