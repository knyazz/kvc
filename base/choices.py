#coding: utf-8
from django.utils.translation import ugettext_lazy as _

EVENT_TAG = (
    ('blue', _(u'Ярмарка')),
    ('orange', _(u'Выставка')),
    ('green', _(u'Заседание')),   
)

PUBLISH_CHOICES = (
    (0, _(u'неактивно')),
    (1, _(u'черновик')),
    (2, _(u'опубликовано')),
)

TEMPLATES = (
    ('object_detail.html', _(u'По умолчанию')),
    ('howto.html', _(u'Как добраться')),
)

SOCIAL_NET_TYPE = (
    ('vkcom', _(u'Вконтакте')),
    ('facebook', _(u'Facebook')),
    ('googleplus', _(u'Google +')),
    ('twitter', _(u'Twitter')),
    ('youtube', _(u'Youtube')),
    ('instagram', _(u'Instagram'))
)

ICON_CLASS = (
    ('', _(u'Без иконок')),
    ('eventscalendaricon', _(u'Календарь')),
    ('schemeicon', _(u'Карта-схема')),
    ('routeicon', _(u'Компас')),
)

SITE_BLOCK_TYPES = (
    (0, _(u'Шапка')),
    (1, _(u'Верхнее меню')),
    (2, _(u'Боковое меню')),
    (3, _(u'Нижнее меню')),
    (4, _(u'Подвал')),
)

STATIC_SITE = (
    (0, _(u'Аккредитация')),
    (1, _(u'Медиа-банк')),
    (2, _(u'Вакансии')),
    (3, _(u'Документация')),
    #(4, _(u'Подвал')),
)

EMAIL_TEMPLATES = (
    (0, _(u'Вакансии. Резюме')),
    (1, _(u'Пресса. Аккредитация')),
    (2, _(u'Онлайн-заявка на организацию мероприятия')),
)