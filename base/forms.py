#coding: utf-8
from django.utils.translation import ugettext_lazy as _

from .fields import LocaleModelSelect2MultipleField, LocaleModelChoiceField


class LocaleFieldModelFormMixin(object):
    def get_locale_field(self, model, request, multiple=True):
        kwargs = dict(  queryset=model.objects.all(),
                        required=False,
                        request=request)
        if multiple:
            return LocaleModelSelect2MultipleField(**kwargs)
        else:
            return LocaleModelChoiceField(**kwargs)

    def get_empty_label(self, request=None):
        return _(u'Любое')