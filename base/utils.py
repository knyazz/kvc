#coding: utf-8
import datetime

from django.conf import settings
from django.core.mail import EmailMessage
from django.template import Context, Template


def generate_email(subject,to,body_tpl,body_context,from_email=None, fl=None):
    from_email = from_email or settings.DEFAULT_FROM_EMAIL
    body_tpl = Template(body_tpl)
    body = body_tpl.render(Context(body_context))
    msg = EmailMessage( subject=subject, to=(to,), body=body,
                        from_email=from_email)
    if fl:
        msg.attach( fl.name,
                    fl.read(),
                    fl.content_type
                )
    return msg


def send_email(subject,
               to,
               recipients=None,
               **kwargs
               ):
    u'''
        Основная celery функция асинхронной отправки email сообщений.

        :param subject: Тема письма
        :type subject: str
        :param to: Заголовок to письма.
        :type to: list
        :param from_email: Адресат письма. По умолчанию `settings.DEFAULT_FROM_EMAIL`
        :type from_email: str
        :param recipients: Список получателей. Если указан,
            то рассылает по одному письму всем получателям,
            соответственно заполняя поле to письма
        :type recipients: list
        :param body_tpl: шаблона письма
        :type body_tpl_path: str
        :param body_context: Контекст шаблона письма
        :type body_context: dict
    '''
    if recipients:
        #for r in recipients:
            #msg = generate_email(subject, to=[r], **kwargs)
            #msg.send()
        to = recipients
    #else:
    msg = generate_email(subject, to, **kwargs)
    msg.send()


def path_modificate(request):
    if request and request.LANGUAGE_CODE:
        if request.path and request.path.split('/')[1] == request.LANGUAGE_CODE:
            return '/'+'/'.join(request.path.split('/')[2:]) #delete locale
    return request.path

def format_locale_date(string, request):
    if request and request.LANGUAGE_CODE:
        mask = {
                'ru': '%d.%m.%Y',
                'en': '%m/%d/%Y',
                'zh-cn': '%Y-%m-%d',
            }.get(request.LANGUAGE_CODE)
        return datetime.datetime.strptime(string,mask)
    return datetime.datetime.strptime(string,'%d.%m.%Y')