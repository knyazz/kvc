from django.db.models import Q
from news.models import NewsSubjects

from .models import SiteBlock, SystemTunes, KVCplan, MenuItem
from .utils import path_modificate


def update_ctx(request, ctx=None):
    ctx = ctx or {}
    path = path_modificate(request)
    if len(path) > 2:
        q = Q(url=path) | Q(url__icontains=path[:-1])
        mi = MenuItem.objects.filter(q).last()
        if mi:
            ctx['parent_menu'] = mi
            mi.has_seo = mi.get_seo(request)
            if mi.parent:
                mi.has_seo = mi.get_seo(request)
                ctx['parent_menu'] = mi.parent
            ctx['current_menuitem'] = mi
    return ctx

def content(request):
    ctx = {
            'footer_block_content': SiteBlock.objects.filter(block_type=4
                                                    ).last(),
            'header_block_content': SiteBlock.objects.filter(block_type=0
                                                    ).last(),
            'main_navigation_block': SiteBlock.objects.filter(block_type=1
                                                    ).last(),
            'side_navigation_menu': SiteBlock.objects.filter(block_type=2
                                                    ).last(),
            'bottom_navigation_menu': SiteBlock.objects.filter(block_type=3
                                                    ).last(),
            'news_subjects': NewsSubjects.objects.filter(parent__isnull=True),
            'systemtune': SystemTunes.objects.filter(is_active=True).last(),
            'kvcplan': KVCplan.objects.filter(is_active=True).last(),
    }
    return update_ctx(request, ctx)