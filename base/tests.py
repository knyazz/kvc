# -*- coding: utf-8 -*-
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.test.client import Client

from django_nose.testcases import FastFixtureTestCase


class BaseTest(FastFixtureTestCase):
    fixtures = (
                'kvc/fixtures/users.json',
                'kvc/fixtures/testcase.json',
            )
    def setUp(self):
        self.test_client = Client()

    @classmethod
    def _fixture_teardown(cls):
        fixtures = getattr(cls, 'fixtures', None)
        _fb_should_teardown_fixtures = getattr(cls, '_fb_should_teardown_fixtures', True)
        # _fixture_teardown needs help with constraints.
        if fixtures and _fb_should_teardown_fixtures:
            call_command('flush', interactive=False, verbosity=1)
        return super(BaseTest, cls)._fixture_teardown()


class MainfuncTest(BaseTest): 
    def base_simple_tests(self):
        '''
            test base functionality
        '''
        # get main page
        response = self.test_client.get(reverse('base:index'))
        self.assertEqual(response.status_code, 200)
        # get success page
        response = self.test_client.get(reverse('base:success'))
        self.assertEqual(response.status_code, 200)
        # get flatpage page url
        link = reverse('base:get_flatpage_url')+'?pk=1'
        response = self.test_client.get(link)
        self.assertEqual(response.status_code, 200)
        # get form page url
        response = self.test_client.get(reverse('base:get_form_url'))
        self.assertEqual(response.status_code, 200)
        # get flat page
        kw = dict(slug='about-company')
        response = self.test_client.get(reverse('base:flat_page', kwargs=kw))
        self.assertEqual(response.status_code, 200)
        # get search page
        response = self.test_client.get(reverse('base:search'))
        self.assertEqual(response.status_code, 200)