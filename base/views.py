#coding: utf-8
import datetime
import itertools
import json
import requests

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.shortcuts import redirect
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import ListView

from calendar_expo.models import Event, BannerItem, MainBigBanner
from dynamic_forms.models import Form
from news.forms import SubscribeEmailForm
from news.models import NewsPage
from partners.models import Partner as PartnerBannerItem

from .models import FlatPage, SliderItem, AbstractPage, EmailTemplate
from .models import SystemTunes, MenuItem
from .utils import send_email


class CTXMixin(object):
    def get_context_data(self, **kwargs):
        ctx = super(CTXMixin, self).get_context_data(**kwargs)
        try:
            ctx['page_title'] = self.model._meta.verbose_name_plural
        except: pass
        ctx['subscribe_form'] = SubscribeEmailForm(self.request)
        return ctx


class IndexPage(CTXMixin, TemplateView):
    u''' Главная страница '''
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        # TODO: need refact
        news = NewsPage.objects.active_pages(self.request)
        if news.count() > 3:
            news =news[:3]
        events = Event.objects.active_pages(self.request
                            ).filter(end_date__gte=datetime.date.today())
        if events.count() > 3:
            events = events[:3]
        sliders = SliderItem.objects.active_pages(self.request
                                    ).filter(is_active=True)
        banners = BannerItem.objects.active(self.request
                                    ).filter(is_active=True,
                                    event__end_date__gte=datetime.date.today())
        partner_banners = PartnerBannerItem.objects.filter(on_main_page=True,
            flr_image__isnull=False)
        if partner_banners.count() > 15:
            partner_banners = partner_banners[:15]
        big_banners = MainBigBanner.objects.active(self.request
                                            ).filter(is_active=True,)
        if big_banners.count() > 2:
            big_banners = big_banners[:2]
        ctx.update({
                    'banners': banners,
                    'big_banners': big_banners,
                    'calendar_events': events,
                    'news': news,
                    'partner_banners': partner_banners,
                    'sliders': sliders,
        })
        return ctx
index = IndexPage.as_view()
success = IndexPage.as_view(template_name='success.html')
test = IndexPage.as_view(template_name='test.html')
plan = IndexPage.as_view(template_name='plan.html')
howto = IndexPage.as_view(template_name='howto.html')

class ListPage(CTXMixin, ListView):
    model = NewsPage
    paginate_by = 10
    template_name = 'basecontent.html'  
news_list = ListPage.as_view()


class DetailPage(CTXMixin, DetailView):
    template_name = 'object_detail.html'
    context_object_name = 'detail_page'

    def get_template_names(self):
        if self.get_object() and self.get_object().template:
            return [self.get_object().template]
        else:
            return super(DetailPage, self).get_template_names()

    def get(self, request, *args, **kwargs):
        if self.get_object() in self.get_queryset().active_pages(request):
            return super(DetailPage, self).get(request, *args, **kwargs)
        else:
            return redirect(reverse('base:index'))
flat_page = DetailPage.as_view(model=FlatPage)


class FlatPageDetail(DetailPage):
    model=FlatPage
    url_kwarg = 'url'

    def get_object(self, queryset=None):    
        if queryset is None:
            queryset = self.get_queryset()
        url = self.kwargs.get(self.url_kwarg, None)
        if not url.startswith('/'): url = '/' + url
        if not url.endswith('/') and settings.APPEND_SLASH: url += '/'
        if url is not None:
            obj = queryset.filter(url_path=url).last()
            if not obj: raise Http404
            return obj
flatpage = FlatPageDetail.as_view()


def check_fp_menuitem_exists(request):
    if request.GET.get('pk'):
        mi = MenuItem.objects.filter(fp=request.GET.get('pk')).last()
        if mi:
            res = dict( exist=True,
                        mi=dict(pk=mi.pk, url=mi.admin_link)
                    )
            return HttpResponse(json.dumps(res),
                                content_type="application/json")
    return HttpResponse(json.dumps(dict(exist=False)), 
                        content_type="application/json")


def get_flatpage_url(request):
    if request.GET.get('pk'):
        fp = FlatPage.objects.filter(pk=request.GET.get('pk')).last()
        if fp:
            return HttpResponse(json.dumps(fp.get_absolute_url()),
                                content_type="application/json")
    return HttpResponse(json.dumps(dict()), 
                        content_type="application/json")

def get_form_url(request):
    if request.GET.get('pk'):
        fp = Form.objects.published(for_user=request.user
                        ).filter(pk=request.GET.get('pk')
                        ).last()
        if fp:
            return HttpResponse(json.dumps(fp.get_absolute_url()),
                                content_type="application/json")
    return HttpResponse(json.dumps(dict()), 
                        content_type="application/json")

def get_flat_list(gen):
    return list(itertools.chain(*gen))

def get_subclass_queryset(model, filters):
    for sub_model in model.__subclasses__():
        if not sub_model.__subclasses__():
            yield sub_model.objects.filter(filters)
        #if sub_model.__subclasses__():
            #yield get_flat_list(get_subclass_queryset(sub_model, filters))
        #else:
            #yield sub_model.objects.filter(filters)

class Search(TemplateView):
    u''' поиск по сайту '''
    template_name = 'search.html'

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        ctx['search_results'] = self.get_pages()
        return ctx

    def get_pages(self):
        if self.request.GET.get('keyword'):
            filters = self.get_filters()
            res = get_subclass_queryset(AbstractPage, filters)
            return get_flat_list(res)

    def get_filters(self):
        dct = self.request.GET.get('keyword', '').split(' ')
        q = None
        for s in dct:
            prefix = {
                        'ru': 'ru',
                        'en': 'en',
                        'zh-cn': 'cn',
                }.get(self.request.LANGUAGE_CODE)
            fields = ['{0}_{1}'.format(prefix, f) for f in ('title', 'content', 'status')]
            expr1 = Q(**{
                            '{0}__{1}'.format(fields[0], 'isnull'): False,
                            fields[2]: 2,
            })
            expr2 = Q(**{'{0}__{1}'.format(fields[0], 'icontains'): s})
            expr3 = Q(**{'{0}__{1}'.format(fields[1], 'icontains'): s})
            expr = (expr1 & (expr2 | expr3))
            if q:
                q|= expr
            else:
                q = expr
        return q
search = Search.as_view()


def get_yandex_probki(request):
    url = 'http://jgo.maps.yandex.net/trf/stat.js?='
    r = requests.get(url)
    if r.status_code == 200:
        res = r.text[r.text.find('[')+1:-4].split('},{')[0][1:]
        res = res[res.find('level:"'):].split('"')[1]
        return HttpResponse(res)
    return HttpResponse(json.dumps(dict()))


class SendEmailMixin(object):
    def send_email(self, obj, email_tpl_type, to_attr):
        email_tpl = EmailTemplate.objects.filter(tpl_type=email_tpl_type).last()
        st = SystemTunes.objects.get_current()
        if email_tpl:
            bctx = dict(obj=obj,)
            send_email( 
                        subject=email_tpl.subject,
                        to = getattr(st,to_attr),
                        body_tpl = email_tpl.text,
                        body_context = bctx,
                        from_email = st.from_email
                    )
