#coding:  utf-8
import csv
import datetime

from django import forms
from django.conf import settings
from django.contrib import admin
from django.http import HttpResponse
from django.core.urlresolvers import resolve
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify


from django_select2.widgets import Select2MultipleWidget
from mptt.admin import MPTTModelAdmin
from mptt.fields import TreeForeignKey
from suit.admin import SortableModelAdmin, SortableTabularInline
from suit.widgets import LinkedSelect, SuitDateWidget

from .models import BaseInfo, FlatPage, SiteBlock, SliderItem, SocialNetwork
from .models import MenuItem, Menu, ExpoType, Subject, SystemTunes, KVCplan
from .models import GreenBlock, EmailTemplate


DEFAULT_FIELDSETS = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ('template',)
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_title', 'ru_status')
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-ru'),
            'fields': ('ru_content',)
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_title', 'en_status')
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-en'),
            'fields': ('en_content',)
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_title', 'cn_status')
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-zh-cn'),
            'fields': ('cn_content',)
        }),
    )

DEFAULT_FORMTABS = (('general', _(u'Общее'),),)+settings.LANGUAGES


class DefaultForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DefaultForm, self).__init__(*args, **kwargs)
        for k,v in self.fields.items():
            if self.fields[k].required:
                v.widget.attrs['required']='required'
            if 'title' in k or 'desc' in k:
                self.fields[k].widget.attrs['style'] = 'width: 100%;'


class AdminBaseMixin(admin.ModelAdmin):
    form = DefaultForm
    formfield_overrides = {
        models.DateField: {'widget': SuitDateWidget},
        models.ForeignKey: {'widget': LinkedSelect},
        models.OneToOneField: {'widget': LinkedSelect},
        TreeForeignKey: {'widget': LinkedSelect},
        models.ManyToManyField: {'widget': Select2MultipleWidget(select2_options={'width': 'resolve'})},
    }
    class Media:
        css = {'all': ('css/jquery-ui-1.10.4.min.css',)}
        js = (
                'js/jquery-ui-1.10.4.min.js',
                #'filebrowser/js/AddFileBrowser.js',
                #'filebrowser/js/FB_CKEditor.js',
                'js/admin.js',
        )

class AdminBaseWithOrder(SortableModelAdmin, AdminBaseMixin):
    sortable='order'

class SystemTuneForm(DefaultForm):
    def __init__(self, *args, **kwargs):
        super(SystemTuneForm, self).__init__(*args, **kwargs)
        fields = (  'ru_title', 'ru_keywords', 'ru_description',
                    'en_title', 'en_keywords', 'en_description',
                    'cn_title', 'cn_keywords', 'cn_description',)
        attrs = dict(style='width:100%;')
        for f in fields:
            field = self.fields.get(f)
            if field:
                attrs['maxlength'] = field.max_length
                field.widget = forms.Textarea(attrs=attrs)


class KVCplanAdmin(AdminBaseMixin):
    suit_form_tabs = DEFAULT_FORMTABS
    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ('is_active',)
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_title', 'ru_image')
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_title', 'en_image')
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_title', 'cn_image')
        }),
    )
admin.site.register(KVCplan, KVCplanAdmin)


class SystemtuneAdmin(AdminBaseMixin):
    form = SystemTuneForm
    suit_form_tabs = (  ('general', _(u'Общее'),),
                        ('emails', _(u'Emails'),),
                        ('seoru', _(u'SEO ru'),),
                        ('seoen', _(u'SEO en'),),
                        ('seocn', _(u'SEO cn'),),
        )
    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'phone', 
                        'sliderspeed', 'is_active', 'google_analytics', 
                        'yandex_metrics')
        }),
        (None, {
            'classes': ('suit-tab suit-tab-emails',),
            'fields': ( 'email', 'to_email', 'server_email', 'accr_email',
                        'vac_email', 'hall_email')
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-seoru',),
            'fields': ('ru_title', 'ru_keywords', 'ru_description')
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-seoen',),
            'fields': ('en_title', 'en_keywords', 'en_description')
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-seocn',),
            'fields': ('cn_title', 'cn_keywords', 'cn_description')
        }),
    )
admin.site.register(SystemTunes, SystemtuneAdmin)

class PageAdmin(AdminBaseMixin):
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super(PageAdmin, self).save_model(request, obj, form, change)


class PageForm(DefaultForm, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PageForm, self).__init__(*args, **kwargs)
        for f in ('keywords', 'description'):
            field = self.fields.get(f)
            if field:
                field.widget.attrs = dict(style='width:100%;',
                                          maxlength = field.max_length)

class SimplePageAdmin(PageAdmin):
    fieldsets = DEFAULT_FIELDSETS
    suit_form_tabs = DEFAULT_FORMTABS
    form = PageForm

class FlatPageAdmin(SimplePageAdmin):
    prepopulated_fields = { 'en_title': ('ru_title',),
                            'cn_title': ('ru_title',),
                            'slug': ('ru_title',)
                        }

    def __init__(self, *args, **kwargs):
        super(FlatPageAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'slug', 'show_date')
            }),
        )

class FlatPageWithSeo(FlatPageAdmin):
    def __init__(self, *args, **kwargs):
        super(FlatPageWithSeo, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'url_path',)
            }),
            ('SEO', {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ('ru_keywords', 'ru_description')
            }),
            ('SEO', {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ('en_keywords', 'en_description')
            }),
            ('SEO', {
                'classes': ('suit-tab suit-tab-cn',),
                'fields': ('cn_keywords', 'cn_description')
            }),
        )
admin.site.register(FlatPage, FlatPageWithSeo)
admin.site.register(BaseInfo, PageAdmin)
admin.site.register(SocialNetwork, PageAdmin)

class MenuItemAdmin(MPTTModelAdmin, SortableModelAdmin, PageAdmin):
    mptt_level_indent = 20
    list_display = ('ru_title',)
    sortable = 'order'
    class Media:
        css = {'all': ('css/jquery-ui-1.10.4.min.css',)}
        js = (
                'js/admin.js',
                'js/jquery-ui-1.10.4.min.js'
        )
    form = SystemTuneForm
    suit_form_tabs = (  ('general', _(u'Общее'),),
                        ('seoru', _(u'SEO ru'),),
                        ('seoen', _(u'SEO en'),),
                        ('seocn', _(u'SEO cn'),),
        )
    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'parent', 'fp', 'form', 'url', 'is_clickable', 'icon',
                        'ru_title', 'en_title', 'cn_title', )
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-seoru',),
            'fields': ('ru_keywords', 'ru_description')
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-seoen',),
            'fields': ('en_keywords', 'en_description')
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-seocn',),
            'fields': ('cn_keywords', 'cn_description')
        }),
    )
admin.site.register(MenuItem, MenuItemAdmin)


class FilterInlineMixin(object):
    class InlineFilterMeta:
        model = None
        inline_model = None

        @classmethod
        def get_q(cls, obj):
            return models.Q()
        
    def formfield_for_dbfield(self, dbfield, **kwargs):
        field = super(FilterInlineMixin, self).formfield_for_dbfield(dbfield, **kwargs)
        request = kwargs.get('request', None)
        config = self.InlineFilterMeta
        if field and hasattr(field, 'queryset') and request:
            if issubclass(field.queryset.model, config.inline_model):
                qs = field.queryset
                args = resolve(request.path)[1]
                obj_id = args[0] if args else None
                if obj_id:
                    obj = config.model.objects.get(id=obj_id)
                    q = config.get_q(obj)
                    if config.order_by: 
                        qs = qs.filter(q).order_by(*config.order_by)
                    field.queryset=qs
        return field


class MenuInline(FilterInlineMixin, SortableTabularInline):
    sortable = 'order'
    model = Menu
    extra = 0
    formfield_overrides = {
        models.DateField: {'widget': SuitDateWidget},
        models.ForeignKey: {'widget': LinkedSelect},
        models.OneToOneField: {'widget': LinkedSelect},
        TreeForeignKey: {'widget': LinkedSelect},
        models.ManyToManyField: {'widget': Select2MultipleWidget(select2_options={'width': 'resolve'})},
    }
        
    class InlineFilterMeta:
        model = SiteBlock
        inline_model = MenuItem
        order_by = ('parent','order','id')
        @classmethod
        def get_q(cls, obj):
            return models.Q(parent__isnull=True)


class SiteBlockAdmin(PageAdmin):
    inlines = (MenuInline,)
    exclude = 'fp',
admin.site.register(SiteBlock, SiteBlockAdmin)

class SliderItemAdmin(SortableModelAdmin, FlatPageAdmin):
    sortable = 'order'
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ('site','is_active',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ('flr_image',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ('en_image',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ('cn_image',)
            }),
        )
admin.site.register(SliderItem, SliderItemAdmin)
admin.site.register(ExpoType)

class SubjectAdmin(SortableModelAdmin, AdminBaseMixin):
    sortable = 'order'
admin.site.register(Subject,SubjectAdmin)

admin.site.register(GreenBlock, AdminBaseMixin)
admin.site.register(EmailTemplate, AdminBaseMixin)


class AdminMakeCSVBase(AdminBaseMixin):
    actions = ('make_csv',)

    def make_csv(self, request, queryset):
        now = datetime.datetime.now()
        response = HttpResponse(mimetype="text/csv")
        fname = self.model._meta.model_name
        fname = "{}-{}.csv".format(fname, slugify(now.ctime()))
        attachment = "attachment; filename=%s" % fname
        response["Content-Disposition"] = attachment
        res = csv.writer(response)
        wrtrow = res.writerow
        title = [unicode(f.verbose_name
                            ).encode('utf-8') for f in self.model._meta.fields
        ]
        wrtrow(title)
        fields = (f.name for f in self.model._meta.fields)
        for row in queryset.values_list(*fields):
            wrtrow([unicode(f).encode('utf-8') for f in row])
        return response
    make_csv.short_description = _(u'Экспортировать выбранное в csv')


class HallVacancyAccrMixin(AdminMakeCSVBase):
    list_filter = ('created',)
    list_display_links = ('id', '__unicode__')
    list_display = list_display_links+list_filter