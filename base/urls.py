from django.conf.urls import patterns, url

urlpatterns = patterns('base.views',
    url(r'^$', 'index', name='index'),
    url(r'^howto/$', 'howto', name='howto'),
    url(r'^success/$', 'success', name='success'),
    url(r'^test/$', 'test', name='test'),
    url(r'^get_flatpage_url/$', 'get_flatpage_url', name='get_flatpage_url'),
    url(r'^get_form_url/$', 'get_form_url', name='get_form_url'),
    url(r'^plan/$', 'plan', name='plan'),
    url(r'^probki_value/$', 'get_yandex_probki', name='get_yandex_probki'),
    url(r'^flatpages/(?P<slug>.*)/$', 'flat_page', name='flat_page'),
    url(r'^search/$', 'search', name='search'),

    
    url(r'^check_fp_menuitem_exists/$', 
        'check_fp_menuitem_exists',
        name='check_fp_menuitem_exists'),
)