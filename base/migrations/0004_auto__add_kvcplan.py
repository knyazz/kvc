# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'KVCplan'
        db.create_table(u'base_kvcplan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ru_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('en_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('cn_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('ru_image', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ru_plans', to=orm['filer.Image'])),
            ('en_image', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='en_plans', null=True, to=orm['filer.Image'])),
            ('cn_image', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cn_plans', null=True, to=orm['filer.Image'])),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'base', ['KVCplan'])


    def backwards(self, orm):
        # Deleting model 'KVCplan'
        db.delete_table(u'base_kvcplan')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'base.baseinfo': {
            'Meta': {'object_name': 'BaseInfo'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'base.expotype': {
            'Meta': {'object_name': 'ExpoType'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'base.flatpage': {
            'Meta': {'object_name': 'FlatPage'},
            'cn_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u53d1\\u5c55\\u4e2d</p>'"}),
            'cn_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'cn_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'cn_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>No description</p>'"}),
            'en_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0434\\u043e\\u0431\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e</p>'"}),
            'ru_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_date': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'object_detail.html'", 'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'base.kvcplan': {
            'Meta': {'object_name': 'KVCplan'},
            'cn_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cn_plans'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'en_plans'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ru_image': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ru_plans'", 'to': "orm['filer.Image']"}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'base.menu': {
            'Meta': {'ordering': "('order', 'siteblock__block_type', 'menuitem__order')", 'unique_together': "(('siteblock', 'menuitem'),)", 'object_name': 'Menu'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menuitem': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu'", 'to': u"orm['base.MenuItem']"}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'siteblock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu'", 'to': u"orm['base.SiteBlock']"})
        },
        u'base.menuitem': {
            'Meta': {'object_name': 'MenuItem'},
            'cn_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'cn_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'form': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['dynamic_forms.Form']", 'null': 'True', 'blank': 'True'}),
            'fp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['base.FlatPage']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['base.MenuItem']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'ru_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2048'})
        },
        u'base.siteblock': {
            'Meta': {'object_name': 'SiteBlock'},
            'block_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'unique': 'True'}),
            'fp': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['base.FlatPage']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['base.BaseInfo']", 'null': 'True', 'blank': 'True'}),
            'net': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['base.SocialNetwork']", 'null': 'True', 'blank': 'True'})
        },
        u'base.slideritem': {
            'Meta': {'object_name': 'SliderItem'},
            'cn_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u53d1\\u5c55\\u4e2d</p>'"}),
            'cn_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>No description</p>'"}),
            'en_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'flr_image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0434\\u043e\\u0431\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e</p>'"}),
            'ru_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_date': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'object_detail.html'", 'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'base.socialnetwork': {
            'Meta': {'object_name': 'SocialNetwork'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'net_type': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'base.subject': {
            'Meta': {'ordering': "('order',)", 'object_name': 'Subject'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'base.systemtunes': {
            'Meta': {'object_name': 'SystemTunes'},
            'cn_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'cn_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'en_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ru_description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sliderspeed': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '10'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'dynamic_forms.form': {
            'Meta': {'object_name': 'Form'},
            'button_text': ('django.db.models.fields.CharField', [], {'default': "u'Submit'", 'max_length': '50'}),
            'email_copies': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'email_from': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'email_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email_subject': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'login_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'response': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'send_email': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sites': ('django.db.models.fields.related.ManyToManyField', [], {'default': '[1]', 'related_name': "u'dynamic_forms_form_forms'", 'symmetrical': 'False', 'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'all_files'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'owned_files'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_filer.file_set'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.folder': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('parent', 'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filer_owned_folders'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['filer.File']},
            '_height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            '_width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'default_caption': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'file_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['filer.File']", 'unique': 'True', 'primary_key': 'True'}),
            'must_always_publish_author_credit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'must_always_publish_copyright': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject_location': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['base']