#coding: utf-8
import os
import pysvg.parser

from django.db import models
from django.utils.translation import ugettext_lazy as _

from filer.models.filemodels import File
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField
from jsonfield import JSONField

from base.models import TitlePage


class Svg(File):
    #file_type = 'Svg'
    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        # the extensions we'll recognise for this file type
        filename_extensions = ['.svg']
        ext = os.path.splitext(iname)[1].lower()
        return ext in filename_extensions

class FilerSvgField(FilerFileField):
    #default_model_class = Svg
    pass


class PlanIcon(TitlePage):
    ru_image = FilerImageField(verbose_name=_(u'Изображение'),
                            related_name='ruplanicons')
    ru_hl_image = FilerImageField(
                            null=True, blank=True,
                            related_name='ruhl_planicons',
                            verbose_name=_(u'Изображение подсвеченное'),
    )
    en_image = FilerImageField(verbose_name=_(u'Изображение'),
                            related_name='enplanicons', null=True, blank=True,)
    en_hl_image = FilerImageField(
                            null=True, blank=True,
                            related_name='enhl_planicons',
                            verbose_name=_(u'Изображение подсвеченное'),
    )
    cn_image = FilerImageField(verbose_name=_(u'Изображение'), 
                            related_name='cnplanicons', null=True, blank=True,)
    cn_hl_image = FilerImageField(
                            null=True, blank=True,
                            related_name='cnhl_planicons',
                            verbose_name=_(u'Изображение подсвеченное'),
    )
    order = models.PositiveIntegerField(_(u'Сортировка'))

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
        return self.ru_image

    def cur_image_url(self, request=None):
        if self.image(request):
            return self.image(request).url

    def hl_image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
        return self.ru_image

    def cur_hl_image_url(self, request=None):
        if self.hl_image(request):
            return self.hl_image(request).url

    class Meta:
        verbose_name= _(u'Иконка для плана')
        verbose_name_plural = _(u'Иконки для плана')

class PlanImage(TitlePage):
    ru_image = FilerImageField(verbose_name=_(u'Изображение'),
                            null=True, blank=True,
                            related_name='ruplanimages')

    en_image = FilerImageField(verbose_name=_(u'Изображение'),
                            null=True, blank=True,
                            related_name='enplanimages')

    cn_image = FilerImageField(verbose_name=_(u'Изображение'),
                            null=True, blank=True,
                            related_name='cnplanimages')
    svg =  FilerSvgField(verbose_name=_(u'Векторное изображение'),
                            null=True, blank=True,
                            related_name='plansvgimages')
    svg_size = models.CharField(_(u'Размер векторного изображения'),
                                blank=True, max_length=255)
    order = models.PositiveIntegerField(_(u'Сортировка'))

    def save(self, *args, **kwargs):
        if self.svg:
            self.svg_size = str(self.get_svg_size())
        super(PlanImage, self).save(*args, **kwargs)

    def get_svg_size(self):
        if self.svg:
            try:
                svg = pysvg.parser.parse(self.svg.file.path)
                return u','.join((svg.get_width(),svg.get_height()))
            except: pass

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
        return self.ru_image

    def cur_image_url(self, request=None):
        if self.image(request):
            return self.image(request).url

    class Meta:
        verbose_name= _(u'Изображение для плана')
        verbose_name_plural = _(u'Изображения для плана')


class Plan(TitlePage):
    data = JSONField(default={})
    order = models.PositiveIntegerField(_(u'Сортировка'))
    is_active = models.BooleanField(_(u'Активный'), default=True)

    class Meta:
        ordering='order', '-pk', 'ru_title'
        verbose_name= _(u'План')
        verbose_name_plural = _(u'Планы')
