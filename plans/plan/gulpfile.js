var gulp = require('gulp');
var gutil = require('gulp-util');
var browserify = require('gulp-browserify');
// var uglify = require('gulp-uglify');

var watch = require('gulp-watch');
var gulpMultinject = require('gulp-multinject');
var clean = require('gulp-rimraf');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var runSequence = require('run-sequence');

var exec = require('child_process').exec;


var staticFolder = 'static/plans/';
var staticFolderPath = '../' + staticFolder;
var templatesFolderPath = '../templates/';

function getBuildName(name) {
  return name + '_' + (new Date()).getTime();
}

var show_build_name = getBuildName('plan_show');
var editor_build_name = getBuildName('plan_editor');
var zoomify_build_name = getBuildName('zoomify');

function configureClean(paths) {
  return gulp.src(paths, {
    read: false
  }).pipe(clean({
    force: true
  }));
}

gulp.task('clean', function() {
  return configureClean([staticFolderPath + '**']);
});

gulp.task('zoomify_clean', function() {
  return configureClean([
    staticFolderPath + 'css/zoomify*',
    staticFolderPath + 'js/zoomify*'
  ]);
});

gulp.task('editor_clean', function() {
  return configureClean([
    staticFolderPath + 'css/plan_editor*',
    staticFolderPath + 'js/plan_editor*'
  ]);
});

gulp.task('show_clean', function() {
  return configureClean([
    staticFolderPath + 'css/plan_show*',
    staticFolderPath + 'js/plan_show*'
  ]);
});

gulp.task('show_template', function() {
  return gulp.src(['../templates/plan_show.html'])
    .pipe(gulpMultinject([
        staticFolder + 'js/' + show_build_name + '.js',
      ],
      'plan_show_js'
    ))
    .pipe(gulpMultinject([
        staticFolder + 'css/' + show_build_name + '.css',
      ],
      'plan_show_css'
    ))
    .pipe(gulp.dest(templatesFolderPath));

});

gulp.task('editor_template', function() {
  return gulp.src(['../templates/change_plan.html'])
    .pipe(gulpMultinject([
        staticFolder + 'js/' + editor_build_name + '.js',
      ],
      'plan_editor_js'
    ))
    .pipe(gulpMultinject([
        staticFolder + 'css/' + editor_build_name + '.css',
      ],
      'plan_editor_css'
    ))
    .pipe(gulp.dest(templatesFolderPath));

});

gulp.task('zoomify_template', function() {
  return gulp.src(['../templates/plan_zoomify_test.html'])
    .pipe(gulpMultinject([
        staticFolder + 'js/' + zoomify_build_name + '.js',
      ],
      'plan_zoomify_js'
    ))
    .pipe(gulpMultinject([
        staticFolder + 'css/' + zoomify_build_name + '.css',
      ],
      'plan_zoomify_css'
    ))
    .pipe(gulp.dest(templatesFolderPath));

});

gulp.task('show_scripts', function() {

  return gulp.src('./node_modules/plan/show/main.js', {
      read: false
    }).pipe(browserify({
      shim: {
        'ol': {
          path: './node_modules/plan/common/vendor/ol3/ol.js',
          exports: null
        }
      },
      debug: false
    }))
    .pipe(rename(show_build_name + '.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(staticFolderPath + '/js'));
});



gulp.task('editor_scripts', function() {

  return gulp.src('./node_modules/plan/editor/main.js', {
      read: false
    }).pipe(browserify({
      transform: ['jstify'],
      shim: {
        'ol': {
          path: './node_modules/plan/common/vendor/ol3/ol.js',
          exports: null
        }
      },
      debug: false
    }))
    .pipe(rename(editor_build_name + '.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(staticFolderPath + '/js'));
});

gulp.task('zoomify_scripts', function() {

  return gulp.src('./node_modules/plan/zoomify_test/main.js', {
      read: false
    }).pipe(browserify({
      shim: {
        'ol': {
          path: './node_modules/plan/common/vendor/ol3/ol.js',
          exports: null
        }
      },
      debug: false
    }))
    .pipe(rename(zoomify_build_name + '.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(staticFolderPath + '/js'));
});

gulp.task('show_styles', function() {
  gulp.src('./src/css/*.css')
    .pipe(concat(show_build_name + '.css'))
    .pipe(gulp.dest(staticFolderPath + '/css'));
});

gulp.task('editor_styles', function() {
  gulp.src('./src/css/*.css')
    .pipe(concat(editor_build_name + '.css'))
    .pipe(gulp.dest(staticFolderPath + '/css'));
});

gulp.task('zoomify_styles', function() {
  gulp.src('./src/css/*.css')
    .pipe(concat(zoomify_build_name + '.css'))
    .pipe(gulp.dest(staticFolderPath + '/css'));
});



gulp.task('default', ['build']);

gulp.task('dev', ['build'], function(cb) {

  gulp.watch(['./node_modules/plan/**/*.*'], function(event) {

    show_build_name = getBuildName('plan_show');
    editor_build_name = getBuildName('plan_editor');
    zoomify_build_name = getBuildName('zoomify');

    runSequence([
      'show',
      'editor',
      'zoomify'
    ], function() {
      exec("touch ../models.py"); //это чтобы manage.py runserver перегружало сервер
    });
  });
});


gulp.task('show', function(cb) {
  runSequence('show_clean', [
    'show_scripts',
    'show_styles',
    'show_template'
  ], cb);
});

gulp.task('editor', function(cb) {
  runSequence('editor_clean', [
    'editor_scripts',
    'editor_styles',
    'editor_template'
  ], cb);
});

gulp.task('zoomify', function(cb) {
  runSequence('zoomify_clean', [
    'zoomify_scripts',
    'zoomify_styles',
    'zoomify_template'
  ], cb);
});


gulp.task('build', function(cb) {
  runSequence([
    'show',
    'editor',
    'zoomify'
  ], cb);
});