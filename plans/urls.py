from django.conf.urls import patterns, url

urlpatterns = patterns('plans.views',
    url(r'^show/$', 'show', name='show'),
    url(r'^zoomify_test/$', 'zoomify_test'),
    url(r'^api/planicons/$', 'api_planicons', name='api_planicons'),
    url(r'^api/planimages/$', 'api_planimages', name='api_planimages'),
    url(r'^api/plans/$', 'api_plans', name='api_plans'),
    url(r'^api/plans/(?P<pk>\d+)/$', 'api_plan_detail', name='api_plan_detail'),
    url(r'^api/plans/actual/$', 'actual_plan_api', name='actual_plan_api'),
)
