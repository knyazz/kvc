#coding: utf-8
from django.contrib import admin

#from mptt.admin import MPTTModelAdmin

from base.admin import AdminBaseWithOrder, DEFAULT_FORMTABS

from .models import PlanIcon, Plan, PlanImage

class PlanIconAdmin(AdminBaseWithOrder):
    suit_form_tabs = DEFAULT_FORMTABS
    fieldsets = (
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_title', 'ru_image','ru_hl_image')
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_title', 'en_image','en_hl_image')
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_title', 'cn_image','cn_hl_image')
        }),
    )
admin.site.register(PlanIcon, PlanIconAdmin)

class PlanImageAdmin(AdminBaseWithOrder):
    suit_form_tabs = DEFAULT_FORMTABS
    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ('svg',)
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_title', 'ru_image')
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_title', 'en_image')
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_title', 'cn_image')
        }),
    )
admin.site.register(PlanImage, PlanImageAdmin)



class PlanAdmin(AdminBaseWithOrder):

    fields = ('ru_title', 'is_active', 'data')
    list_display = ('ru_title', 'is_active')
    change_form_template = 'change_plan.html'
admin.site.register(Plan, PlanAdmin)
