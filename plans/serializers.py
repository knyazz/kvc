#coding: utf-8
from rest_framework import serializers

from base.serializers import JSONField

from .models import PlanIcon, Plan, PlanImage


class PlanIconSerializer(serializers.ModelSerializer):
    def to_native(self, obj):
        data = super(PlanIconSerializer, self).to_native(obj)
        data['title'] = obj.title(self.context.get('request'))
        _request = self.context.get('request')
        for fld in ('image', 'hl_image'):
            _img = getattr(obj, fld)(_request) or getattr(obj, fld)()
            if _img:
                data[fld+'_size'] = (_img.width, _img.height)
                data[fld+'_url'] = _img.url
        return data

    class Meta:
        model = PlanIcon
        fields = 'id',

class PlanImageSerializer(serializers.ModelSerializer):
    svg_url = serializers.Field(source="svg.url")

    def to_native(self, obj):
        data = super(PlanImageSerializer, self).to_native(obj)
        _request = self.context.get('request')
        data['title'] = obj.title(_request)
        _img = obj.image(_request) or obj.image()
        if _img:
            data['image_size'] = (_img.width, _img.height)
            data['image_url'] = _img.url
        svg = getattr(obj, 'svg')
        if not obj.svg_size and svg:
            data['svg_size'] = obj.get_svg_size()
        return data

    class Meta:
        model = PlanImage
        fields = 'id', 'svg_url', 'svg_size'


class PlanSerializer(serializers.ModelSerializer):
    data = JSONField(source="data")

    def to_native(self, obj):
        data = super(PlanSerializer, self).to_native(obj)
        data['title'] = obj.title(self.context.get('request'))
        return data

    class Meta:
        model = Plan
        fields = 'id', 'data'
