#coding: utf-8
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from rest_framework import generics

from .models import PlanIcon, PlanImage, Plan
from .serializers import PlanIconSerializer, PlanImageSerializer, PlanSerializer

class PlanShow(TemplateView):
    template_name = 'plan_show.html'
show = PlanShow.as_view()

class PlanZoomifyTest(TemplateView):
    template_name = 'plan_zoomify_test.html'
zoomify_test = PlanZoomifyTest.as_view()


class PlanIconAPIList(generics.ListAPIView):
    model = PlanIcon
    serializer_class = PlanIconSerializer
api_planicons = PlanIconAPIList.as_view()


class PlanImageAPIList(generics.ListAPIView):
    model = PlanImage
    serializer_class = PlanImageSerializer

    @method_decorator(never_cache)
    def get(self, request, *args, **kwargs):
        return super(PlanImageAPIList, self).get(request, args, kwargs)
api_planimages = PlanImageAPIList.as_view()


class PlanAPIList(generics.ListAPIView):
    model = Plan
    serializer_class = PlanSerializer
api_plans = PlanAPIList.as_view()


class PlanAPIDetail(generics.RetrieveUpdateAPIView):
    model = Plan
    serializer_class = PlanSerializer
api_plan_detail = PlanAPIDetail.as_view()


class ActualPlanAPI(PlanAPIDetail):
    queryset=Plan.objects.filter(is_active=True)
    def get_object(self, queryset=None):
        return self.queryset.last()
actual_plan_api = ActualPlanAPI.as_view()
