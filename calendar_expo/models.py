# coding: utf-8
import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from easy_thumbnails.files import get_thumbnailer
from filer.fields.image import FilerImageField

from base.defaults import SITE_HELP_TEXT
from base.models import AbstractPage, Subject, ExpoType, LocaleAttrMixin

from .choices import BANNER_COLORS
from .managers import BannerManager


class Event(LocaleAttrMixin, AbstractPage):
    u''' календарь событий '''
    start_date = models.DateField(_(u'начало'),default=datetime.date.today())
    end_date = models.DateField(_(u'окончание'),default=datetime.date.today())
    subjects = models.ManyToManyField(Subject, null=True, blank=True,
                                verbose_name=Subject._meta.verbose_name_plural)
    expotype = models.ForeignKey(ExpoType, null=True, blank=True,
                                verbose_name=_(u'Тип мероприятия'))
    #russian
    ru_venue = models.CharField(_(u'место проведения'), max_length=255)
    ru_org = models.CharField(_(u'организатор'),max_length=255)
    ru_site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT)
    ru_reglink = models.URLField(_(u'Регистрация'), help_text=SITE_HELP_TEXT,
                                blank=True)
    ru_image = FilerImageField(verbose_name=_(u'Изображение'),
                                null=True, blank=True,
                                related_name='ru_events')
    #english
    en_venue = models.CharField(_(u'место проведения'), max_length=255,
                                blank=True,)
    en_org = models.CharField(_(u'организатор'),max_length=255, blank=True,)
    en_site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT,
                                blank=True,)
    en_reglink = models.URLField(_(u'Регистрация'), help_text=SITE_HELP_TEXT,
                                blank=True)
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True,blank=True,
                                related_name='en_events')
    #chinese
    cn_venue = models.CharField(_(u'место проведения'), max_length=255,
                                blank=True,)
    cn_org = models.CharField(_(u'организатор'),max_length=255, blank=True,)
    cn_site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT,
                                blank=True,)
    cn_reglink = models.URLField(_(u'Регистрация'), help_text=SITE_HELP_TEXT,
                                blank=True)
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True,blank=True,
                                related_name='cn_events')
    order = models.PositiveIntegerField(_(u'Сортировка'), default=0)

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            image = {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
            if image:
                opts = dict(size=(265,190))
                thumbnail = get_thumbnailer(image).get_thumbnail(opts)
                return thumbnail
            
    def image_url(self, request=None):
        if self.image(request):
            return self.image(request).url
        return settings.STATIC_URL+'images/SuperE_265x190.png'
    
    class Meta:
        ordering = 'start_date', 'end_date', 'order',
        verbose_name = _(u'событие календаря')
        verbose_name_plural = _(u'календарь событий')

    def get_absolute_url(self):
        return reverse('calendar:events')+'?slug='+self.slug


class BannerItem(LocaleAttrMixin, models.Model):
    objects = BannerManager()
    event = models.OneToOneField(Event, verbose_name=Event._meta.verbose_name)
    is_active = models.BooleanField(_(u'Показать на главной'), default=True)
    order = models.PositiveIntegerField(_(u'Сортировка'), default=0)

    ru_title = models.CharField(_(u'Заголовок для главной'), max_length=255,
                            blank=True)
    ru_desc = models.CharField(_(u'Описание для главной'), max_length=255,
                            blank=True)
    ru_image = FilerImageField(verbose_name=_(u'Изображение'),
                                null=True, blank=True,
                                related_name='ru_banneritem')

    en_title = models.CharField(_(u'Заголовок для главной'), max_length=255,
                            blank=True)
    en_desc = models.CharField(_(u'Описание для главной'), max_length=255,
                            blank=True)
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True,blank=True,
                                related_name='en_banneritem')

    cn_title = models.CharField(_(u'Заголовок для главной'), max_length=255,
                            blank=True)
    cn_desc = models.CharField(_(u'Описание для главной'), max_length=255,
                            blank=True)
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True,blank=True,
                                related_name='cn_banneritem')
    color = models.CharField(_(u'Цвет блока'), choices=BANNER_COLORS,
                                    default='green', max_length=30)

    def __unicode__(self):
        return u'{0} {1}'.format(self._meta.verbose_name, self.event)

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            image = {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
            if image:
                opts = dict(size=(287,140))
                thumbnail = get_thumbnailer(image).get_thumbnail(opts)
                return thumbnail
            
    def image_url(self, request=None):
        if self.image(request):
            return self.image(request).url
        return settings.STATIC_URL+'images/SuperE_287x140.png'

    class Meta:
        ordering = 'order',
        verbose_name= _(u'Баннер')
        verbose_name_plural = _(u'Баннеры')


class MainBigBanner(LocaleAttrMixin, models.Model):
    objects = BannerManager()
    ru_site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT, blank=True)
    ru_image = FilerImageField(verbose_name=_(u'Изображение'),
                                related_name='ru_bigbanneritem')

    en_site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT, blank=True)
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True,blank=True,
                                related_name='en_bigbanneritem')

    cn_site = models.URLField(_(u'сайт'), help_text=SITE_HELP_TEXT, blank=True)
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True,blank=True,
                                related_name='cn_bigbanneritem')
    event = models.OneToOneField(Event, verbose_name=Event._meta.verbose_name,
                                blank=True, null=True)
    is_active = models.BooleanField(_(u'Показать на главной'), default=True)

    def __unicode__(self):
        return u'{0} {1}'.format(self._meta.verbose_name, self.event)

    def image(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_image,
                    'en': self.en_image,
                    'zh-cn': self.cn_image,
            }.get(request.LANGUAGE_CODE)
            #if image:
                #opts = dict(size=(596,288))
                #thumbnail = get_thumbnailer(image).get_thumbnail(opts)
                #return thumbnail

    def image_url(self, request=None):
        return self.image(request) and self.image(request).url

    def event_url(self,request=None):
        return  self.get_locale_attr('site', request) or (
                self.event and self.event.get_locale_attr('site', request))

    class Meta:
        ordering='-pk',
        verbose_name= _(u'Большой Баннер на главной')
        verbose_name_plural = _(u'Большие Баннеры на главной')