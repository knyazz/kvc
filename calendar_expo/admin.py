#coding:  utf-8
from django.contrib import admin

from suit.admin import SortableModelAdmin

from base.admin import FlatPageAdmin, AdminBaseMixin, DEFAULT_FORMTABS

from .models import Event, BannerItem, MainBigBanner


class EventAdmin(SortableModelAdmin, FlatPageAdmin):
    sortable = 'order'
    list_display_links = ('id','__unicode__')
    list_display = list_display_links + ('start_date', 'end_date')
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'subjects', 'expotype', 'start_date', 'end_date')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ( 'ru_venue', 'ru_org', 'ru_site', 'ru_reglink',
                            'ru_image', )
            }),
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ( 'en_venue', 'en_org', 'en_site', 'en_reglink',
                            'en_image',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ( 'cn_venue', 'cn_org', 'cn_site', 'cn_reglink',
                            'cn_image',)
            }),
        )

admin.site.register(Event, EventAdmin)

class BannerAdmin(SortableModelAdmin, AdminBaseMixin):
    sortable = 'order'
    fieldsets = (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'event', 'is_active')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ( 'ru_title', 'ru_desc',
                            'ru_image', )
            }),
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ( 'en_title', 'en_desc',
                            'en_image',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ( 'cn_title', 'cn_desc',
                            'cn_image',)
            }),
        )
    suit_form_tabs = DEFAULT_FORMTABS
admin.site.register(BannerItem,BannerAdmin)

class MainBigBannerAdmin(AdminBaseMixin):
    fieldsets = (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'event', 'is_active')
            }),
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ( 'ru_site', 'ru_image', )
            }),
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ( 'en_site', 'en_image',)
            }),
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ( 'cn_site', 'cn_image',)
            }),
        )
    suit_form_tabs = DEFAULT_FORMTABS
admin.site.register(MainBigBanner,MainBigBannerAdmin)