from django.conf.urls import patterns, url

urlpatterns = patterns('calendar_expo.views',
    url(r'^$', 'events', name='events'),
    url(r'^api/event_titles/$', 'event_title_list', name='event_title_list'),
    url(r'^api/events/$', 'api_event_list', name='api_event_list'),
    url(r'^(?P<slug>.*)/ics/$', 'event_ics', name='event_ics'),
    url(r'^(?P<slug>.*)/csv/$', 'event_csv', name='event_csv'),
    url(r'^(?P<slug>.*)/$', 'detail', name='detail'),
)