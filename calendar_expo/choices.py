#coding: utf-8
from django.utils.translation import ugettext_lazy as _

EVENT_SUBJECTS = (
    (0, _(u'Автомобили')),
    (1, _(u'Строительство')),
    (2, _(u'Одежда')),
    (3, _(u'Ювелирные изделия')),
    (4, _(u'Информационные технологии')),
    (5, _(u'Промышленность')),
)

BANNER_COLORS = (
    ('green', _(u'Зеленый')),
    ('lightblue', _(u'Голубой')),
    ('blue', _(u'Синий')),
)