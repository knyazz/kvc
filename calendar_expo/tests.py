# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class CalendarTest(BaseTest):
    def calendar_simple_tests(self):
        '''
            test calendar functionality
        '''
        # get main page
        response = self.test_client.get(reverse('calendar:events'))
        self.assertEqual(response.status_code, 200)
        # get events list page
        response = self.test_client.get(reverse('calendar:event_title_list'))
        self.assertEqual(response.status_code, 200)
        # get api events list page
        response = self.test_client.get(reverse('calendar:api_event_list'))
        self.assertEqual(response.status_code, 200)
        kw = dict(slug='agrorus_expo')
        # get event csv
        response = self.test_client.get(reverse('calendar:event_ics', kwargs=kw))
        self.assertEqual(response.status_code, 200)
        # get event csv
        response = self.test_client.get(reverse('calendar:event_csv', kwargs=kw))
        self.assertEqual(response.status_code, 200)
        # get event page
        response = self.test_client.get(reverse('calendar:detail', kwargs=kw))
        self.assertEqual(response.status_code, 200)