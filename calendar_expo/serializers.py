#from django.http import QueryDict
from django.template import Context
from django.template.loader import get_template
from django.templatetags.l10n import localize

from rest_framework import serializers
 
from base.paginators import CustomPaginationSerializer

from .models import Event


class EventTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = 'id', 'subjects', 'expotype', 'slug'

    def to_native(self, obj):
        data = super(EventTitleSerializer, self).to_native(obj)
        data['title'] = obj.title(self.context.get('request'))
        return data


class EventPaginationSerializer(CustomPaginationSerializer):
    pass


class EventSerializer(EventTitleSerializer):
    image_url = serializers.Field(source="image_url")
    tag = serializers.Field(source="expotype.tag")
    start_date = serializers.SerializerMethodField('get_local_start_date')
    def get_local_start_date(self,obj):
        return localize(obj.start_date)

    end_date = serializers.SerializerMethodField('get_local_end_date')
    def get_local_end_date(self,obj):
        return localize(obj.end_date)

    def to_native(self, obj):
        data = super(EventSerializer, self).to_native(obj)
        data['content'] = obj.content(self.context.get('request'))
        if obj.expotype:
            data['get_tag_display'] = obj.expotype.title(self.context.get('request'))
        data['html'] = self.event_html(obj)
        return data

    def event_html(self, obj):
        tpl = get_template('partial/event_block.html')
        ctx = dict(request = self.context.get('request'), event = obj)
        return tpl.render(Context(ctx))

    class Meta(EventTitleSerializer.Meta):
        fields = 'id', 'tag', 'image_url', 'slug', 'start_date', 'end_date',