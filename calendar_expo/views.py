#coding: utf-8
import csv
import datetime
import icalendar

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView

from rest_framework import generics

from base.views import DetailPage, CTXMixin
from base.utils import format_locale_date

from .forms import EventForm
from .models import Event
from .serializers import EventTitleSerializer, EventSerializer
from .serializers import EventPaginationSerializer


class RestEvents(generics.ListAPIView):
    model = Event
    serializer_class = EventTitleSerializer

    def filter_queryset(self, queryset):
        qs = super(RestEvents, self).filter_queryset(queryset)
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('term'):
                k = body.get('term')
                kq = (  
                        Q(ru_title__icontains=k) |
                        Q(en_title__icontains=k) |
                        Q(cn_title__icontains=k)
                    )
                q&= kq
            if body.getlist('id'):
                k = body.getlist('id')
                q&= Q(id__in=k)
        return qs.filter(q)
event_title_list = RestEvents.as_view()


class EventMixin(CTXMixin):
    model = Event
    paginate_by = 5

    def get_queryset(self):
        qs = super(EventMixin, self).get_queryset(
                                       ).active_pages(self.request)
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('expotype'):
                q&= Q(expotype=body.get('expotype'))
            if body.getlist('subjects'):
                q&= Q(subjects__isnull=False) & Q(subjects__in=body.getlist('subjects'))
            if body.get('keyword'):
                k = body.get('keyword')
                kq = (  #Q(subjects__contains=k) | 
                        Q(ru_title__icontains=k) |
                        Q(en_title__icontains=k) |
                        Q(cn_title__icontains=k) |
                        Q(ru_content__icontains=k) |
                        Q(en_content__icontains=k) |
                        Q(cn_content__icontains=k)
                )
                q&= kq
            if body.get('start_date'):
                date = format_locale_date(body.get('start_date'),self.request)
                q&= Q(end_date__gte=date)
            if body.get('end_date'):                
                date = format_locale_date(body.get('end_date'),self.request)
                q&= Q(start_date__lte=date)
            if body.get('slug'):
                q&=Q(slug=body.get('slug'))
            if not body.get('start_date') and not body.get('end_date'):
                q&=Q(end_date__gte=datetime.date.today())
        else:
            q&=Q(end_date__gte=datetime.date.today())
        ids = qs.filter(q).values_list('pk', flat=True)
        return qs.filter(pk__in=set(ids))


class APIEventsList(EventMixin, generics.ListAPIView):
    serializer_class = EventSerializer
    pagination_serializer_class = EventPaginationSerializer
api_event_list = APIEventsList.as_view()


class Events(EventMixin, ListView):
    u''' страница событий календаря'''
    template_name = 'events_list.html'

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        ctx['event_form'] = EventForm(self.request, self.request.GET)
        return ctx
events = Events.as_view()

detail = DetailPage.as_view(model=Event)

def event_csv(request, slug):
    event = get_object_or_404(Event, slug=slug)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="{0}.csv"'.format(slug)
    writer = csv.writer(response)
    writer.writerow([   'Subject', 
                        'Start Date', 'Start Time',
                        'End Date', 'End Time', 
                        'All Day Event', 'Description',
                        'Location', 'Private'
                    ])
    subj = event.expotype.title(request).encode('utf-8') if event.expotype else ''
    subj = subj+ ' '.join([s.title(request).encode('utf-8') for s in event.subjects.all()])
    start_date = event.start_date.strftime('%Y/%m/%d').encode('utf-8')
    end_date = event.end_date.strftime('%Y/%m/%d').encode('utf-8')
    writer.writerow([   subj,
                        start_date, '10:00:00',
                        end_date, '19.00.00',
                        True, event.title(request).encode('utf-8'),
                        event.get_locale_attr('venue', request
                            ).encode('utf-8'), False
                    ])

    return response


def event_ics(request, slug):
    event = get_object_or_404(Event, slug=slug)
    cal = icalendar.Calendar()
    __event = icalendar.Event()
    subj = event.expotype.title(request).encode('utf-8') if event.expotype else ''
    subj = subj+ ' '.join([s.title(request).encode('utf-8') for s in event.subjects.all()])
    start_date = event.start_date.strftime('%Y%m%d').encode('utf-8')+'T100000'
    end_date = event.end_date.strftime('%Y%m%d').encode('utf-8')+'T190000'
    __event['dtstart'] = start_date
    __event['dtend'] = end_date
    __event['summary'] = subj
    __event['description'] = event.title(request).encode('utf-8')
    __event['location'] = event.get_locale_attr('venue', request).encode('utf-8')
    cal.add_component(__event)
    response = HttpResponse(cal.to_ical(), content_type='text/calendar; charset=utf8')
    response['Content-Disposition'] = 'attachment; filename="{0}.ics"'.format(slug)
    return response