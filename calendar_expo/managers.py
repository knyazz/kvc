# coding: utf-8
from django.db import models


class BannerQuerySet(models.query.QuerySet):
    def active(self,request=None):
        if request:
            filt = {
                    'ru': {},
                    'en': {'en_image__isnull': False,},
                    'zh-cn': {'cn_image__isnull': False,},
            }.get(request.LANGUAGE_CODE)
            return self.filter(models.Q(**filt))
        else:
            return self

class BannerManager(models.Manager):
    def get_queryset(self):
        return BannerQuerySet(self.model, using=self._db)

    def active(self, request=None):
        return self.get_queryset().active(request)