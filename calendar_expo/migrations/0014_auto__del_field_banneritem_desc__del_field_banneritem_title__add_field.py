# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'BannerItem.desc'
        db.rename_column(u'calendar_expo_banneritem', 'desc', 'ru_desc')

        # Deleting field 'BannerItem.title'
        db.rename_column(u'calendar_expo_banneritem', 'title', 'ru_title')

        # Adding field 'BannerItem.en_title'
        db.add_column(u'calendar_expo_banneritem', 'en_title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'BannerItem.en_desc'
        db.add_column(u'calendar_expo_banneritem', 'en_desc',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'BannerItem.cn_title'
        db.add_column(u'calendar_expo_banneritem', 'cn_title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'BannerItem.cn_desc'
        db.add_column(u'calendar_expo_banneritem', 'cn_desc',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Deleting field 'MainBigBanner.site'
        db.rename_column(u'calendar_expo_mainbigbanner', 'site', 'ru_site')

        # Adding field 'MainBigBanner.en_site'
        db.add_column(u'calendar_expo_mainbigbanner', 'en_site',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'MainBigBanner.cn_site'
        db.add_column(u'calendar_expo_mainbigbanner', 'cn_site',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200, blank=True),
                      keep_default=False)


    def backwards(self, orm):

        # Deleting field 'BannerItem.ru_title'
        db.rename_column(u'calendar_expo_banneritem', 'ru_title', 'title')

        # Deleting field 'BannerItem.ru_desc'
        db.rename_column(u'calendar_expo_banneritem', 'ru_desc', 'desc')

        # Deleting field 'BannerItem.en_title'
        db.delete_column(u'calendar_expo_banneritem', 'en_title')

        # Deleting field 'BannerItem.en_desc'
        db.delete_column(u'calendar_expo_banneritem', 'en_desc')

        # Deleting field 'BannerItem.cn_title'
        db.delete_column(u'calendar_expo_banneritem', 'cn_title')

        # Deleting field 'BannerItem.cn_desc'
        db.delete_column(u'calendar_expo_banneritem', 'cn_desc')

        # Deleting field 'MainBigBanner.ru_site'
        db.rename_column(u'calendar_expo_mainbigbanner', 'ru_site', 'site')

        # Deleting field 'MainBigBanner.en_site'
        db.delete_column(u'calendar_expo_mainbigbanner', 'en_site')

        # Deleting field 'MainBigBanner.cn_site'
        db.delete_column(u'calendar_expo_mainbigbanner', 'cn_site')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'base.expotype': {
            'Meta': {'object_name': 'ExpoType'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'base.subject': {
            'Meta': {'ordering': "('order',)", 'object_name': 'Subject'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'calendar_expo.banneritem': {
            'Meta': {'ordering': "('order',)", 'object_name': 'BannerItem'},
            'cn_desc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cn_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cn_banneritem'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'color': ('django.db.models.fields.CharField', [], {'default': "'green'", 'max_length': '30'}),
            'en_desc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'en_banneritem'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['calendar_expo.Event']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ru_desc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'ru_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ru_banneritem'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'calendar_expo.event': {
            'Meta': {'ordering': "('start_date', 'end_date', 'order')", 'object_name': 'Event'},
            'cn_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u53d1\\u5c55\\u4e2d</p>'"}),
            'cn_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cn_events'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'cn_org': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cn_reglink': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'cn_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'cn_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cn_venue': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>No description</p>'"}),
            'en_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'en_events'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'en_org': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_reglink': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'en_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'en_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_venue': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 10, 30, 0, 0)'}),
            'expotype': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['base.ExpoType']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0434\\u043e\\u0431\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e</p>'"}),
            'ru_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ru_events'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'ru_org': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ru_reglink': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'ru_site': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'ru_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ru_venue': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_date': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 10, 30, 0, 0)'}),
            'subjects': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['base.Subject']", 'null': 'True', 'blank': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'object_detail.html'", 'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'calendar_expo.mainbigbanner': {
            'Meta': {'ordering': "('-pk',)", 'object_name': 'MainBigBanner'},
            'cn_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cn_bigbanneritem'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'cn_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'en_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'en_bigbanneritem'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'en_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['calendar_expo.Event']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ru_image': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ru_bigbanneritem'", 'to': "orm['filer.Image']"}),
            'ru_site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'all_files'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'owned_files'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_filer.file_set'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.folder': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('parent', 'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filer_owned_folders'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['filer.File']},
            '_height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            '_width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'default_caption': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'file_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['filer.File']", 'unique': 'True', 'primary_key': 'True'}),
            'must_always_publish_author_credit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'must_always_publish_copyright': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject_location': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '64', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['calendar_expo']