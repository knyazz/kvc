# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class OrganizersTest(BaseTest):
    def organizers_simple_tests(self):
        '''
            test organizers functionality
        '''
        # get halls page
        response = self.test_client.get(reverse('organizers:halls'))
        self.assertEqual(response.status_code, 200)
        # get halls order page
        response = self.test_client.get(reverse('organizers:hall_order'))
        self.assertEqual(response.status_code, 200)
        # post hall order
        data = {
                'title': 'test',
                'capacity': 50,
                'hall': 1,
                'contact_name': 'test',
                'contact_phone': 'test',
                'contact_email': 'test',
                'desc': 'test',
        }
        response = self.test_client.post(reverse('organizers:hall_order'), data)
        self.assertEqual(response.status_code, 200)