#coding: utf-8
from django.contrib import admin

from base.admin import FlatPageAdmin, HallVacancyAccrMixin

from .models import House, Hall, HallOrder, SeatingType


admin.site.register(SeatingType)
admin.site.register(House, FlatPageAdmin)

class ImagesInLine(admin.TabularInline):
    model = Hall.images.through
    extra = 0
    verbose_name=u'Изображение зала'
    verbose_name_plural = u'Изображения зала'

class HallAdmin(FlatPageAdmin):
    inlines = (ImagesInLine,)
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets+= (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'house', 'seating_type', 'capacity')
        }),)
admin.site.register(Hall, HallAdmin)


class HallOrderAdmin(HallVacancyAccrMixin):
    pass
admin.site.register(HallOrder, HallOrderAdmin)