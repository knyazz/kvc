from django.conf.urls import patterns, url

urlpatterns = patterns('organizers_app.views',
    url(r'^halls/$', 'halls', name='halls'),
    url(r'^hall/(?P<slug>.*)/$', 'hall', name='hall'),
    url(r'^ordering/hall/$', 'hall_order', name='hall_order'),
    url(r'^ordering/hall/(?P<hall_slug>.*)/$', 'hall_order', name='hall_order'),
)