#coding: utf-8
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.views.generic import CreateView, ListView
from django.shortcuts import get_object_or_404

from base.models import ExpoType
from base.views import CTXMixin, DetailPage, SendEmailMixin

from .forms import HallOrderForm
from .models import Hall, HallOrder#, SeatingType

class HallsPage(CTXMixin, ListView):
    model = Hall
    paginate_by = 10
    template_name = 'halls.html'

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        ctx['titles'] = self.model.objects.active_pages(self.request)
        ctx['seating_types'] = ExpoType.objects.all()
        ctx['capacity'] = Hall.objects.values_list('capacity', flat=True)
        return ctx

    def get_queryset(self):
        qs = super(self.__class__, self).get_queryset(
                                       ).active_pages(self.request)
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.getlist('seaing_type__pk'):
                q&= Q(seaing_type__pk__in=body.getlist('seaing_type__pk'))
            if body.get('capacity'):
                q&= Q(capacity__in=body.getlist('capacity'))
        return qs.filter(q)
halls = HallsPage.as_view()
hall = DetailPage.as_view(model=Hall)

class HallOrdering(CTXMixin, SendEmailMixin, CreateView):
    model = HallOrder
    template_name = 'hallorder_form.html'
    form_class = HallOrderForm

    def get_initial(self):
        initial=super(HallOrdering, self).get_initial()
        if self.request.GET.get('room'):
            initial['hall'] = get_object_or_404(Hall, 
                                            pk=self.request.GET.get('room')
                            )
        return initial

    def get_success_url(self):
        obj = self.object
        obj.page_title = u'Онлайн-заявка на организацию мероприятия'
        self.send_email(obj, email_tpl_type=2,to_attr='hall_to')
        return reverse('base:success')
hall_order = HallOrdering.as_view()