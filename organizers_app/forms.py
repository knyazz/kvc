#coding: utf-8
from django import forms

from .models import HallOrder


class HallOrderForm(forms.ModelForm):
    class Meta:
        model = HallOrder

    def __init__(self, *args, **kwargs):
        super(HallOrderForm, self).__init__(*args, **kwargs)
        self.fields['hall'].empty_label=None
        for k,v in self.fields.items():
            if self.fields[k].required:
                v.widget.attrs['required']='required'
            if k == 'capacity':
                v.initial = None