# coding: utf-8
import datetime

from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from base.models import AbstractPage, TitlePage

#from .choices import CAPACITY


class House(AbstractPage):
    class Meta:
        verbose_name = _(u'Здание')
        verbose_name_plural = _(u'Здания')


class SeatingType(TitlePage):
    class Meta:
        verbose_name=_(u'Тип рассадки')
        verbose_name_plural=_(u'Типы рассадки')

class Hall(AbstractPage):
    house = models.ForeignKey(House, verbose_name=House._meta.verbose_name,
                            null=True, blank=True)
    seating_type = models.ForeignKey(SeatingType, null=True, blank=True,
                                    verbose_name=_(u'Тип рассадки'))
    capacity = models.PositiveIntegerField(_(u'Вместимость зала, чел'),
                                            default=0)
    images = models.ManyToManyField('filestorage.Mediafile', null=True)
    
    class Meta:
        verbose_name = _(u'Зал')
        verbose_name_plural = _(u'Залы')

    def get_absolute_url(self):
        return reverse('organizers:hall', kwargs=dict(slug=self.slug))


class HallOrder(models.Model):
    title = models.CharField(_(u'Название мероприятия'), max_length=255)
    capacity = models.CharField(_(u'Количество участников'), max_length=255,
                                blank=True, null=True)
                                            #choices=CAPACITY)
    start_date = models.DateField(_(u'начало'),default=datetime.date.today())
    end_date = models.DateField(_(u'окончание'),default=datetime.date.today())
    hall = models.ForeignKey(Hall, verbose_name=Hall._meta.verbose_name,
                            null=True, blank=True)
    extra_need = models.BooleanField(
        _(u'Необходимы дополнительные услуги'), default=False, 
        help_text=_(u'Например: кейтеринг, аренда оборудования, перевод')
    )
    contact_name = models.CharField(_(u'Имя'), max_length=255)
    contact_phone = models.CharField(_(u'Телефон'), max_length=255)
    contact_email = models.EmailField(_(u'Email'))
    desc = models.TextField(_(u'Дополнительное описание мероприятия'),
                                max_length=2048, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'{0} {1}'.format(self.title, self.created)

    class Meta:
        ordering = '-created', 'start_date', 'end_date', 'hall'
        verbose_name = verbose_name_plural =_(u'Бронирование зала')

    @property
    def admin_link(self):
        if self.pk:
            return reverse('admin:organizers_app_hallorder_change',
                            args=(self.pk,),)
    @property
    def absolute_admin_link(self):
        return 'http://'+Site.objects.get_current().domain+self.admin_link