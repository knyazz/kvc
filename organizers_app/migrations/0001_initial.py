# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'House'
        db.create_table(u'organizers_app_house', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ru_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('en_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('cn_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('template', self.gf('django.db.models.fields.CharField')(default='object_detail.html', max_length=255)),
            ('ru_content', self.gf('ckeditor.fields.RichTextField')(default=u'<p>\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0435\u0449\u0451 \u043d\u0435 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u043e</p>')),
            ('ru_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=2)),
            ('en_content', self.gf('ckeditor.fields.RichTextField')(default=u'<p>No description</p>')),
            ('en_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=2)),
            ('cn_content', self.gf('ckeditor.fields.RichTextField')(default=u'<p>\u53d1\u5c55\u4e2d</p>')),
            ('cn_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=2)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=255)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=2048, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=2048, blank=True)),
            ('show_date', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'organizers_app', ['House'])

        # Adding model 'SeatingType'
        db.create_table(u'organizers_app_seatingtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ru_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('en_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('cn_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'organizers_app', ['SeatingType'])

        # Adding model 'Hall'
        db.create_table(u'organizers_app_hall', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ru_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('en_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('cn_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('template', self.gf('django.db.models.fields.CharField')(default='object_detail.html', max_length=255)),
            ('ru_content', self.gf('ckeditor.fields.RichTextField')(default=u'<p>\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0435\u0449\u0451 \u043d\u0435 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u043e</p>')),
            ('ru_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=2)),
            ('en_content', self.gf('ckeditor.fields.RichTextField')(default=u'<p>No description</p>')),
            ('en_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=2)),
            ('cn_content', self.gf('ckeditor.fields.RichTextField')(default=u'<p>\u53d1\u5c55\u4e2d</p>')),
            ('cn_status', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=2)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=255)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=2048, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=2048, blank=True)),
            ('show_date', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('house', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizers_app.House'], null=True, blank=True)),
            ('seating_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizers_app.SeatingType'], null=True, blank=True)),
            ('capacity', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'organizers_app', ['Hall'])

        # Adding M2M table for field images on 'Hall'
        m2m_table_name = db.shorten_name(u'organizers_app_hall_images')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('hall', models.ForeignKey(orm[u'organizers_app.hall'], null=False)),
            ('mediafile', models.ForeignKey(orm[u'filestorage.mediafile'], null=False))
        ))
        db.create_unique(m2m_table_name, ['hall_id', 'mediafile_id'])

        # Adding model 'HallOrder'
        db.create_table(u'organizers_app_hallorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('capacity', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0, null=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 7, 22, 0, 0))),
            ('end_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 7, 22, 0, 0))),
            ('hall', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['organizers_app.Hall'])),
            ('extra_need', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('contact_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('contact_phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('contact_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('desc', self.gf('django.db.models.fields.TextField')(max_length=2048, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'organizers_app', ['HallOrder'])


    def backwards(self, orm):
        # Deleting model 'House'
        db.delete_table(u'organizers_app_house')

        # Deleting model 'SeatingType'
        db.delete_table(u'organizers_app_seatingtype')

        # Deleting model 'Hall'
        db.delete_table(u'organizers_app_hall')

        # Removing M2M table for field images on 'Hall'
        db.delete_table(db.shorten_name(u'organizers_app_hall_images'))

        # Deleting model 'HallOrder'
        db.delete_table(u'organizers_app_hallorder')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'base.expotype': {
            'Meta': {'object_name': 'ExpoType'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'base.subject': {
            'Meta': {'object_name': 'Subject'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'all_files'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'owned_files'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_filer.file_set'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.folder': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('parent', 'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filer_owned_folders'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['filer.File']},
            '_height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            '_width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'default_caption': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'file_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['filer.File']", 'unique': 'True', 'primary_key': 'True'}),
            'must_always_publish_author_credit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'must_always_publish_copyright': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject_location': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'filestorage.gallery': {
            'Meta': {'object_name': 'Gallery'},
            'cn_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u53d1\\u5c55\\u4e2d</p>'"}),
            'cn_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>No description</p>'"}),
            'en_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0434\\u043e\\u0431\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e</p>'"}),
            'ru_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_date': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'subjects': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['base.Subject']", 'null': 'True', 'blank': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['base.ExpoType']", 'null': 'True', 'blank': 'True'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'object_detail.html'", 'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'filestorage.mediafile': {
            'Meta': {'object_name': 'Mediafile'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'fl_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.Gallery']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']", 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'video': ('embed_video.fields.EmbedVideoField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'organizers_app.hall': {
            'Meta': {'object_name': 'Hall'},
            'capacity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cn_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u53d1\\u5c55\\u4e2d</p>'"}),
            'cn_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>No description</p>'"}),
            'en_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'house': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['organizers_app.House']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['filestorage.Mediafile']", 'null': 'True', 'symmetrical': 'False'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0434\\u043e\\u0431\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e</p>'"}),
            'ru_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'seating_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['organizers_app.SeatingType']", 'null': 'True', 'blank': 'True'}),
            'show_date': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'object_detail.html'", 'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'organizers_app.hallorder': {
            'Meta': {'ordering': "('-created', 'start_date', 'end_date', 'hall')", 'object_name': 'HallOrder'},
            'capacity': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'contact_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'contact_phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.TextField', [], {'max_length': '2048', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 7, 22, 0, 0)'}),
            'extra_need': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hall': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['organizers_app.Hall']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 7, 22, 0, 0)'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'organizers_app.house': {
            'Meta': {'object_name': 'House'},
            'cn_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u53d1\\u5c55\\u4e2d</p>'"}),
            'cn_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>No description</p>'"}),
            'en_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '2048', 'blank': 'True'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'default': "u'<p>\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435 \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0434\\u043e\\u0431\\u0430\\u0432\\u043b\\u0435\\u043d\\u043e</p>'"}),
            'ru_status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_date': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'}),
            'template': ('django.db.models.fields.CharField', [], {'default': "'object_detail.html'", 'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'organizers_app.seatingtype': {
            'Meta': {'object_name': 'SeatingType'},
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['organizers_app']