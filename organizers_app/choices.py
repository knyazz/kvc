#coding: utf-8
from django.utils.translation import ugettext_lazy as _

CAPACITY = (
    (0, _(u'любое')),
    (1, _(u'Менее 50')),
    (2, _(u'Более 50')),
)