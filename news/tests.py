# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class NewsTest(BaseTest):
    def news_simple_tests(self):
        '''
            test news functionality
        '''
        # get news list page
        response = self.test_client.get(reverse('news:news_list'))
        self.assertEqual(response.status_code, 200)
        # get api news list page
        response = self.test_client.get(reverse('news:api_news_list'))
        self.assertEqual(response.status_code, 200)
        # subscribe
        data = {
                'subjects': 1,
                'event': 1,
                'email': 'test@test.com',
        }
        response = self.test_client.post(reverse('news:subscribe'), data)
        self.assertEqual(response.status_code, 201)
        # get mediafile page
        kw = dict(slug='newspage_testovaya')
        response = self.test_client.get(reverse('news:detail', kwargs=kw))
        self.assertEqual(response.status_code, 200)