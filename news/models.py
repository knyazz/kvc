# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from base.models import AbstractPage


class NewsSubjects(MPTTModel):
    u''' Тематики новостей '''
    ru_title = models.CharField(_(u'Заголовок (рус.)'), max_length=255)
    en_title = models.CharField(_(u'Заголовок (англ.)'), max_length=255)
    cn_title = models.CharField(_(u'Заголовок (кит.)'), max_length=255)
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children', verbose_name=_(u'родитель'))
    order = models.PositiveIntegerField(_(u'Сортировка'))

    class MPTTMeta:
        order_insertion_by = ('order',)

    def save(self, *args, **kwargs):
        super(self.__class__, self).save(*args, **kwargs)
        self.__class__.objects.rebuild()

    def __unicode__(self):
        return self.title()

    def title(self, request=None):
        if request:
            return {
                    'ru': self.ru_title,
                    'en': self.en_title,
                    'zh-cn': self.cn_title,
            }.get(request.LANGUAGE_CODE)
        return self.ru_title

    def get_pages(self,request=None):
        if not self.parent:
            qs = self.newspage_set.model.objects.filter(subjects__parent=self)
            if request:
                qs = qs.active_pages(request)
            return qs

    class Meta:
        verbose_name = _(u'Тематика новостей')
        verbose_name_plural = _(u'Тематики новостей')


class NewsPage(AbstractPage):
    u''' Новости портала '''
    subjects = models.ForeignKey(NewsSubjects, verbose_name=_(u'Тематика'))
    event = models.ForeignKey('calendar_expo.Event',
                            verbose_name=_(u'Выставка'), null=True, blank=True)

    class Meta:
        ordering = ('-created',)
        verbose_name= _(u'Новости портала')
        verbose_name_plural = verbose_name

    def get_absolute_url(self):
        return reverse('news:detail', kwargs=dict(slug=self.slug))


class SubscribeEmail(models.Model):
    subjects = models.ForeignKey(NewsSubjects, verbose_name=_(u'Тематика'),
                                null=True, blank=True)
    event = models.ForeignKey('calendar_expo.Event', verbose_name=_(u'Выставка'),
                                null=True, blank=True)
    email = models.EmailField(_('email'))

    __unicode__ = lambda self: self.email

    class Meta:
        verbose_name=_(u'Подписка новостей')
        verbose_name_plural = verbose_name