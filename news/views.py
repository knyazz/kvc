#coding: utf-8
from django.db.models import Q
from django.views.generic import DetailView
from django.views.generic.list import ListView

from rest_framework import generics

from base.views import CTXMixin

from .models import NewsPage, NewsSubjects, SubscribeEmail
from .serializers import NewsPageSerializer

class NewsMixin(CTXMixin):
    u''' страница новостей'''
    template_name = 'news_list.html'
    model = NewsPage
    paginate_by = 3

    def get_queryset(self):
        qs = super(NewsMixin, self).get_queryset(
                                       ).active_pages(self.request)
        #TODO: refact this
        q = Q()
        body = self.request.GET or getattr(self.request, 'QUERY_PARAMS', None)
        if body:
            if body.getlist('pk'):
                q&= Q(pk__in=body.getlist('pk'))
            if body.getlist('subjects__pk'):
                q&= (   Q(subjects__pk__in=body.getlist('subjects__pk')) |
                        (   Q(subjects__parent__isnull=False) &
                            Q(subjects__parent__pk__in=body.getlist('subjects__pk'))
                        )
                    )
        return qs.filter(q)


class APINewsList(NewsMixin, generics.ListAPIView):
    u''' '''
    serializer_class = NewsPageSerializer
api_news_list = APINewsList.as_view()

class NewsList(NewsMixin, ListView):
    u''' страница новостей'''
    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        ctx['titles'] = self.model.objects.active_pages(self.request)
        subjects = NewsSubjects.objects.all()
        ctx['subjects'] = subjects.filter(parent__isnull=True)
        return ctx
news_list = NewsList.as_view()

class NewsDetail(CTXMixin, DetailView):
    model=NewsPage
    template_name='news_detail.html'
    context_object_name = 'detail_page'
detail = NewsDetail.as_view()


class SubscribeForm(generics.CreateAPIView):
    model = SubscribeEmail
subscribe = SubscribeForm.as_view()