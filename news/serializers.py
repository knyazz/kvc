from django.templatetags.l10n import localize
from rest_framework import serializers
 
from .models import NewsPage


class NewsPageSerializer(serializers.ModelSerializer):
    url = serializers.Field(source="get_absolute_url")
    #created = serializers.DateTimeField(format='%d %B %Y %H:%M:%S')
    created = serializers.SerializerMethodField('get_local_datetime')
    def get_local_datetime(self,obj):
        return localize(obj.created)
    class Meta:
        model = NewsPage
        fields = 'id', 'created', 'show_date', 'url'

    def to_native(self, obj):
        data = super(NewsPageSerializer, self).to_native(obj)
        data['title'] = obj.title(self.context.get('request'))
        data['content'] = obj.content(self.context.get('request'))
        return data