#coding: utf-8
from django import forms

from base.forms import LocaleFieldModelFormMixin
from calendar_expo.models import Event

from .models import SubscribeEmail, NewsSubjects


class SubscribeEmailForm(LocaleFieldModelFormMixin, forms.ModelForm):
    class Meta:
        model = SubscribeEmail

    def __init__(self, request=None, *args, **kwargs):
        super(SubscribeEmailForm, self).__init__(*args, **kwargs)
        self.fields['event'] = self.get_locale_field(Event,request, 
                                                    multiple=False)
        self.fields['subjects'] = self.get_locale_field(NewsSubjects,request, 
                                                        multiple=False)