#coding: utf-8
from django.contrib import admin

from suit.admin import SortableModelAdmin
from mptt.admin import MPTTModelAdmin

from base.admin import FlatPageAdmin, AdminBaseMixin

from .models import NewsPage, NewsSubjects, SubscribeEmail


class NewsSubjectsAdmin(AdminBaseMixin, MPTTModelAdmin, SortableModelAdmin):
    mptt_level_indent = 20
    list_display = ('ru_title',)
    sortable = 'order'

admin.site.register(NewsSubjects, NewsSubjectsAdmin)

class NewsPageAdmin(FlatPageAdmin):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets+= (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'subjects', 'event', 'created')
        }),)
admin.site.register(NewsPage, NewsPageAdmin)

admin.site.register(SubscribeEmail, AdminBaseMixin)