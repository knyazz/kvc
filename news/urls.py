from django.conf.urls import patterns, url

urlpatterns = patterns('news.views',
    url(r'^$', 'news_list', name='news_list'),
    url(r'^api/list/$', 'api_news_list', name='api_news_list'),
    url(r'^api/subscribe/$', 'subscribe', name='subscribe'),
    url(r'^(?P<slug>.*)/$', 'detail', name='detail'),
)