#coding: utf-8
from django.views.generic import DetailView
from django.utils.translation import ugettext_lazy as _

from .models import HowtoPage

class HowtoPage(DetailView):
    template_name = 'model_howto.html'
    context_object_name = 'detail_page'
    model = HowtoPage
    queryset = model.objects.filter(is_active=True)

    def get_context_data(self, **kwargs):
        ctx = super(HowtoPage, self).get_context_data(**kwargs)
        try:
            ctx['page_title'] = _(u'Как добраться')
        except: pass
        return ctx

    def get_object(self, queryset=None):
        return self.get_queryset().last()
howtopage = HowtoPage.as_view()