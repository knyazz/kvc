#coding: utf-8
from django.conf import settings
from django.contrib import admin

from base.admin import DEFAULT_FORMTABS, AdminBaseMixin

from .models import HowtoPage, HowToAutoBlock, HowtoTrafficBlock, Bus
from .models import AccordionBlock, HowtoTrafficContent


HOWTOBASE_FIELDSETS = (
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_title',)
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-ru'),
            'fields': ('ru_content',)
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_title',)
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-en'),
            'fields': ('en_content',)
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_title',)
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-zh-cn'),
            'fields': ('cn_content',)
        }),
    )


class HowToAutoBlockAdmin(AdminBaseMixin):
    fieldsets = HOWTOBASE_FIELDSETS
    suit_form_tabs = settings.LANGUAGES

    def __init__(self, *args, **kwargs):
        super(HowToAutoBlockAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            # русский
            (None, {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ('ru_image',)
            }),
            #английский 
            (None, {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ('en_image',)
            }),
            #китайский
            (None, {
                'classes': ('suit-tab suit-tab-zh-cn',),
                'fields': ('cn_image',)
            }),
        )
admin.site.register(HowToAutoBlock, HowToAutoBlockAdmin)


class HowtoTrafficBlockAdmin(AdminBaseMixin):
    pass
admin.site.register(HowtoTrafficBlock, HowtoTrafficBlockAdmin)


class HowtoPageAdmin(AdminBaseMixin):
    fieldsets = HOWTOBASE_FIELDSETS
    suit_form_tabs = DEFAULT_FORMTABS

    def __init__(self, *args, **kwargs):
        super(HowtoPageAdmin, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            (None, {
                'classes': ('suit-tab suit-tab-general',),
                'fields': ( 'has_map', 'autoblock', 'traffblock', 'is_active')
            }),
        )
admin.site.register(HowtoPage, HowtoPageAdmin)

class BusAdmin(AdminBaseMixin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'tp',)
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_name', 'ru_desc')
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_name', 'en_desc')
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_name', 'cn_desc')
        }),
    )
    suit_form_tabs = DEFAULT_FORMTABS
admin.site.register(Bus, BusAdmin)

admin.site.register(AccordionBlock)
admin.site.register(HowtoTrafficContent)