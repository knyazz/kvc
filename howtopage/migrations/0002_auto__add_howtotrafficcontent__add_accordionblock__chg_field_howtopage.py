# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HowtoTrafficContent'
        db.create_table(u'howtopage_howtotrafficcontent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ru_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('en_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('cn_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'howtopage', ['HowtoTrafficContent'])

        # Adding M2M table for field acc_blocks on 'HowtoTrafficContent'
        m2m_table_name = db.shorten_name(u'howtopage_howtotrafficcontent_acc_blocks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('howtotrafficcontent', models.ForeignKey(orm[u'howtopage.howtotrafficcontent'], null=False)),
            ('accordionblock', models.ForeignKey(orm[u'howtopage.accordionblock'], null=False))
        ))
        db.create_unique(m2m_table_name, ['howtotrafficcontent_id', 'accordionblock_id'])

        # Adding model 'AccordionBlock'
        db.create_table(u'howtopage_accordionblock', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ru_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('en_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('cn_title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'howtopage', ['AccordionBlock'])

        # Adding M2M table for field blocks on 'AccordionBlock'
        m2m_table_name = db.shorten_name(u'howtopage_accordionblock_blocks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('accordionblock', models.ForeignKey(orm[u'howtopage.accordionblock'], null=False)),
            ('howtotrafficblock', models.ForeignKey(orm[u'howtopage.howtotrafficblock'], null=False))
        ))
        db.create_unique(m2m_table_name, ['accordionblock_id', 'howtotrafficblock_id'])


        # Changing field 'HowtoPage.traffblock'
        db.alter_column(u'howtopage_howtopage', 'traffblock_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['howtopage.HowtoTrafficContent'], null=True))

    def backwards(self, orm):
        # Deleting model 'HowtoTrafficContent'
        db.delete_table(u'howtopage_howtotrafficcontent')

        # Removing M2M table for field acc_blocks on 'HowtoTrafficContent'
        db.delete_table(db.shorten_name(u'howtopage_howtotrafficcontent_acc_blocks'))

        # Deleting model 'AccordionBlock'
        db.delete_table(u'howtopage_accordionblock')

        # Removing M2M table for field blocks on 'AccordionBlock'
        db.delete_table(db.shorten_name(u'howtopage_accordionblock_blocks'))


        # Changing field 'HowtoPage.traffblock'
        db.alter_column(u'howtopage_howtopage', 'traffblock_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['howtopage.HowtoTrafficBlock'], null=True))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'all_files'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'owned_files'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_filer.file_set'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.folder': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('parent', 'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filer_owned_folders'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['filer.File']},
            '_height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            '_width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'default_caption': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'file_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['filer.File']", 'unique': 'True', 'primary_key': 'True'}),
            'must_always_publish_author_credit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'must_always_publish_copyright': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject_location': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'howtopage.accordionblock': {
            'Meta': {'object_name': 'AccordionBlock'},
            'blocks': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['howtopage.HowtoTrafficBlock']", 'null': 'True', 'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'howtopage.bus': {
            'Meta': {'object_name': 'Bus'},
            'desc': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tp': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        u'howtopage.howtoautoblock': {
            'Meta': {'object_name': 'HowToAutoBlock'},
            'cn_content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'ru_image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']", 'null': 'True', 'blank': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'howtopage.howtopage': {
            'Meta': {'object_name': 'HowtoPage'},
            'autoblock': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['howtopage.HowToAutoBlock']", 'null': 'True', 'blank': 'True'}),
            'cn_content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'has_map': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ru_content': ('ckeditor.fields.RichTextField', [], {'blank': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'traffblock': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['howtopage.HowtoTrafficContent']", 'null': 'True', 'blank': 'True'})
        },
        u'howtopage.howtotrafficblock': {
            'Meta': {'object_name': 'HowtoTrafficBlock'},
            'buses': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['howtopage.Bus']", 'null': 'True', 'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['filer.Image']", 'null': 'True', 'blank': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'howtopage.howtotrafficcontent': {
            'Meta': {'object_name': 'HowtoTrafficContent'},
            'acc_blocks': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['howtopage.AccordionBlock']", 'null': 'True', 'blank': 'True'}),
            'cn_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'en_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ru_title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['howtopage']