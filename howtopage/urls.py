from django.conf.urls import patterns, url

urlpatterns = patterns('howtopage.views',
    url(r'^$', 'howtopage', name='howtopage'),
)