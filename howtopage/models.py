# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField
from filer.fields.image import FilerImageField

from base.defaults import SITE_HELP_TEXT
from base.models import TitlePage,  LocaleAttrMixin


class HowToAutoBlock(LocaleAttrMixin, TitlePage):
    ru_image = FilerImageField(verbose_name=_(u'Изображение (рус.)'),
                                null=True, blank=True,
                                related_name='howtoautoruflr')
    en_image = FilerImageField(verbose_name=_(u'Изображение (англ.)'),
                                null=True, blank=True,
                                related_name='howtoautoenflr')
    cn_image = FilerImageField(verbose_name=_(u'Изображение (кит.)'),
                                null=True, blank=True,
                                related_name='howtoautocnflr')
    image_link = models.URLField(_(u'Ссылка изображения'),
                                help_text=SITE_HELP_TEXT, blank=True)
    ru_content = RichTextField(_(u'Содержание (рус.)'), blank=True)
    en_content = RichTextField(_(u'Содержание (англ.)'), blank=True)
    cn_content = RichTextField(_(u'Содержание (кит.)'), blank=True)
    def content(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_content,
                    'en': self.en_content,
                    'zh-cn': self.cn_content,
            }.get(request.LANGUAGE_CODE)
        return self.ru_content

    class Meta:
        verbose_name=_(u'''Блок "на машине"''')
        verbose_name_plural=verbose_name


class Bus(models.Model):
    ru_name = models.CharField(_(u'Номера'), max_length=255)
    ru_desc = RichTextField(_(u'Доп описание'), blank=True)
    en_name = models.CharField(_(u'Номера'), max_length=255, blank=True)
    en_desc = RichTextField(_(u'Доп описание'), blank=True)
    cn_name = models.CharField(_(u'Номера'), max_length=255, blank=True)
    cn_desc = RichTextField(_(u'Доп описание'), blank=True)
    tp = models.CharField(_(u'Тип транспорта'), max_length=16,
                            choices=(   ('busicon', _(u'Автобус')),
                                        ('trallicon', _(u'Троллейбус')),
                                        ('furgonicon', _(u'Маршрутка')),
                                    )
                        )
    def get_locale_attr(self, attr, request=None):
        cd = 'ru'
        if request and request.LANGUAGE_CODE:
            cd = request.LANGUAGE_CODE
        return getattr(self, cd+'_'+attr)

    __unicode__ = lambda self: self.get_locale_attr('name')
    class Meta:
        verbose_name=_(u'Как добраться. Транспорт')
        verbose_name_plural=verbose_name



class HowtoTrafficBlock(TitlePage):
    ru_image = FilerImageField(verbose_name=_(u'Изображение (рус.)'),
                                null=True, blank=True)
    image_link = models.URLField(_(u'Ссылка изображения'),
                                help_text=SITE_HELP_TEXT, blank=True)
    buses = models.ManyToManyField(Bus, null=True, blank=True,
                                verbose_name=Bus._meta.verbose_name_plural)
    class Meta:
        verbose_name=_(u'''Запись общ. транспорта''')
        verbose_name_plural=_(u'''Записи общ. транспорта''')


class AccordionBlock(TitlePage):
    blocks = models.ManyToManyField(HowtoTrafficBlock, null=True, blank=True,
                    verbose_name=HowtoTrafficBlock._meta.verbose_name_plural)
    class Meta:
        verbose_name=_(u'''Значение аккордиона''')
        verbose_name_plural=_(u'''Значения аккордиона''')


class HowtoTrafficContent(TitlePage):
    acc_blocks = models.ManyToManyField(AccordionBlock, null=True, blank=True,
                        verbose_name=AccordionBlock._meta.verbose_name_plural)
    class Meta:
        verbose_name=_(u'''Блок "общ. транспорт"''')
        verbose_name_plural=verbose_name


class HowtoPage(TitlePage):
    is_active = models.BooleanField(_(u'Активный'), default=True)
    ru_content = RichTextField(_(u'Содержание желтого блока (рус.)'), blank=True)
    en_content = RichTextField(_(u'Содержание желтого блока (англ.)'), blank=True)
    cn_content = RichTextField(_(u'Содержание желтого блока (кит.)'), blank=True)
    has_map = models.BooleanField(_(u'Наличие yandex карты'), default=True)
    autoblock = models.ForeignKey(HowToAutoBlock, null=True, blank=True,
                                verbose_name=HowToAutoBlock._meta.verbose_name)
    traffblock = models.ForeignKey(HowtoTrafficContent, null=True, blank=True,
                            verbose_name=HowtoTrafficContent._meta.verbose_name)

    def content(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_content,
                    'en': self.en_content,
                    'zh-cn': self.cn_content,
            }.get(request.LANGUAGE_CODE)
        return self.ru_content

    class Meta:
        verbose_name=_(u'Страница')
        verbose_name_plural=_(u'Страницы')