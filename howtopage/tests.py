# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class HowtoTest(BaseTest):
    def howtopage_simple_tests(self):
        '''
            test howtopage functionality
        '''
        # get howtopage page
        response = self.test_client.get(reverse('howtopage:howtopage'))
        self.assertEqual(response.status_code, 200)