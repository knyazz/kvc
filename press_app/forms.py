#coding: utf-8
from django import forms
#from django.utils.translation import ugettext_lazy as _

from .models import AccreditationOrder


class AccreditationOrderForm(forms.ModelForm):
    opts = dict(required='required',)
    class Meta:
        model = AccreditationOrder

    def __init__(self, *args, **kwargs):
        super(AccreditationOrderForm, self).__init__(*args, **kwargs)
        #self.fields['expotype'].empty_label=('', _(u'любая')),
        for k,v in self.fields.items():
            if self.fields[k].required:
                v.widget.attrs=self.opts