#coding: utf-8
from django.core.urlresolvers import reverse
from django.views.generic import CreateView

from base.models import GreenBlock
from base.views import CTXMixin, SendEmailMixin

from .forms import AccreditationOrderForm
from .models import AccreditationOrder


class AccreditationOrdering(CTXMixin, SendEmailMixin, CreateView):
    model = AccreditationOrder
    form_class = AccreditationOrderForm
    template_name = 'accreditationorder_form.html'

    def get_success_url(self):
        obj = self.object
        obj.page_title = u'Аккредитация'
        self.send_email(obj, email_tpl_type=1,to_attr='accr_to')
        return reverse('base:success')

    def get_context_data(self, **kwargs):
        ctx = super(AccreditationOrdering, self).get_context_data(**kwargs)
        ctx['greenblock'] = GreenBlock.objects.filter(page=0).last()
        return ctx
accreditation_order = AccreditationOrdering.as_view()
