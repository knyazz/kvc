from django.conf.urls import patterns, url

urlpatterns = patterns('press_app.views',
    url(r'^ordering/accreditation/$',
        'accreditation_order',
        name='accreditation_order'
    ),
)