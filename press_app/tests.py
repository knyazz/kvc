# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class PressTest(BaseTest):
    def press_simple_tests(self):
        '''
            test press functionality
        '''
        # get accreditation_order page
        response = self.test_client.get(reverse('press:accreditation_order'))
        self.assertEqual(response.status_code, 200)
        # post accreditation_order
        data = {
                'name': 'test',
                'fio': 'test',
                'post': 'test',
                'contact_phone': 'test',
                'contact_email': 'test@test.com',
                'office_phone': 'test',
                'office_email': 'test@test.com',
                'editor_fio': 'test',
                'action': 'test',
        }
        response = self.test_client.post(reverse('press:accreditation_order'), data)
        self.assertEqual(response.status_code, 302)