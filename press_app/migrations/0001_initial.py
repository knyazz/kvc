# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AccreditationOrder'
        db.create_table(u'press_app_accreditationorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('fio', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('post', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('contact_phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('contact_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('office_phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('office_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('editor_fio', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'press_app', ['AccreditationOrder'])


    def backwards(self, orm):
        # Deleting model 'AccreditationOrder'
        db.delete_table(u'press_app_accreditationorder')


    models = {
        u'press_app.accreditationorder': {
            'Meta': {'ordering': "('-created', 'name')", 'object_name': 'AccreditationOrder'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'contact_phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'editor_fio': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fio': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'office_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'office_phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'post': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['press_app']