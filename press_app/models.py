# coding: utf-8
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _


class AccreditationOrder(models.Model):
    name = models.CharField(_(u'СМИ'), max_length=255)
    action = models.CharField(_(u'Мероприятие'), max_length=255,)
    fio = models.CharField(_(u'ФИО'), max_length=255,)
    post = models.CharField(_(u'Должность'), max_length=255,)
    contact_phone = models.CharField(_(u'Контактный телефон'), max_length=255,)
    contact_email = models.EmailField(_(u'Контактный email'))
    office_phone = models.CharField(_(u'Телефон редакции'), max_length=255,)
    office_email = models.EmailField(_(u'Email редакции'))
    editor_fio = models.CharField(_(u'ФИО главного редактора'), max_length=255,)
    created = models.DateTimeField(auto_now_add=True)

    __unicode__ = lambda self: self.name

    class Meta:
        ordering = '-created', 'name'
        verbose_name = verbose_name_plural =_(u'Аккредитация')


    @property
    def admin_link(self):
        if self.pk:
            return reverse('admin:press_app_accreditationorder_change',
                            args=(self.pk,),)

    @property
    def absolute_admin_link(self):
        return 'http://'+Site.objects.get_current().domain+self.admin_link