from django.contrib import admin

from base.admin import HallVacancyAccrMixin

from .models import AccreditationOrder


class AccreditationOrderAdmin(HallVacancyAccrMixin):
    pass
admin.site.register(AccreditationOrder, AccreditationOrderAdmin)