from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import forms_builder.forms.urls

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', name='sitemap_xml'),
    url(r'^jsi18n/(?P<packages>\S+?)/$', 'django.views.i18n.javascript_catalog'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns('',
    url(r'^$', 'base.views.index', name='index'),
    url(r'^current/', include('base.urls', namespace='base')),
    url(r'^company/', include('company.urls', namespace='company')),
    url(r'^calendar/', include('calendar_expo.urls', namespace='calendar')),
    url(r'^dynamic_forms/', include('dynamic_forms.urls', namespace='dynamic_forms')),
    url(r'^filestorage/', include('filestorage.urls', namespace='filestorage')),
    url(r'^howtopage/', include('howtopage.urls', namespace='howtopage')),
    url(r'^news/', include('news.urls', namespace='news')),
    url(r'^organizers/', include('organizers_app.urls', namespace='organizers')),
    url(r'^press/', include('press_app.urls', namespace='press')),
    url(r'^partners/', include('partners.urls', namespace='partners')),
    url(r'^plans/', include('plans.urls', namespace='plans')),
    url(r'^publications/', include('publications.urls', namespace='publications')),
)

urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^forms/', include(forms_builder.forms.urls)),
    (r'^ckeditor/', include('ckeditor.urls')),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


    url(r'^$', 'base.views.index', name='i18n_index'),
)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^rosetta/', include('rosetta.urls')),
    )

urlpatterns += patterns('',
    url(r'^(?P<url>.*)/$', 'base.views.flatpage', name='base.views.flatpage'),
)

urlpatterns += i18n_patterns('',
    url(r'^(?P<url>.*)/$', 'base.views.flatpage', name='i18n_flatpage'),
)