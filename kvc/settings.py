
#coding: utf-8
from django.utils.translation import ugettext_lazy as _

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY = 'aopd=yo(wd#&jhvq!8ixh)=y+ur01poth3vxr_($tv3qhj0r-j'

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = (
    'expoforum-center.ru',
    'kvc.indev-group.eu',
    'kvc-dev.indev-group.eu'
)

SITE_ID = 1

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'ckeditor',
    'django_jenkins',
    #'django_mobile',
    'django_select2',
    'easy_thumbnails',
    'embed_video',
    'filer',
    'forms_builder.forms',
    'mathfilters',
    'mptt',
    'pytils',
    'raven.contrib.django.raven_compat',
    'rest_framework',
    'rosetta',
    'south',
    'suit_ckeditor',
    'widget_tweaks',

    'base',
    'dynamic_forms',
    'calendar_expo',
    'company',
    'filestorage',
    'howtopage',
    'news',
    'organizers_app',
    'press_app',
    'partners',
    'plans',
    'publications',
)

JENKINS_TASKS = (
                    'django_jenkins.tasks.run_pylint',
                    'django_jenkins.tasks.run_pep8',
                    'django_jenkins.tasks.run_pyflakes',
                    #'django_jenkins.tasks.with_coverage',
                    #'django_jenkins.tasks.django_tests',
                )

PROJECT_APPS = INSTALLED_APPS

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',

    'django_mobile.context_processors.flavour',

    'base.context_processors.content',
)

if DEBUG:
    TEMPLATE_CONTEXT_PROCESSORS+= ('django.core.context_processors.debug',)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    #'django_mobile.middleware.MobileDetectionMiddleware',
    #'django_mobile.middleware.SetFlavourMiddleware',

    #'base.middleware.SidebarMenu',
)


ROOT_URLCONF = 'kvc.urls'

WSGI_APPLICATION = 'kvc.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ATOMIC_REQUESTS': True
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        #'LOCATION': 'unix:/tmp/memcached.sock',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': 'KVC',
        'TIMEOUT': 500,
        'BINARY': True,
        'OPTIONS': {
            'tcp_nodelay': True,
            'ketama': True
        }
    }
}

LANGUAGE_CODE = 'ru'
LANGUAGES = (
                ('ru', _('Russian')),
                ('en', _('English')),
                #('zh-cn', _('Chinese')),
)
LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

DEFAULT_FROM_EMAIL = 'noreply@expoforum-center.ru'
DEFAULT_TO_EMAIL = 'noreply@expoforum-center.ru'

SUIT_CONFIG = {
    'ADMIN_NAME': _('KVC'),
    'CONFIRM_UNSAVED_CHANGES': True,
    'MENU_EXCLUDE': ('auth', 'sites'),
    'MENU': (
        {'app': 'base', 'label': _(u'Общее'),},
        {'app': 'dynamic_forms', 'label': _(u'Формы'),},
        {'app': 'filestorage', 'label': _(u'Файлы'),},
        {'app': 'howtopage', 'label': _(u'Как добраться. Модели'),},
        {'app': 'calendar_expo', 'label': _(u'Календарь событий'),},
        {'app': 'company', 'label': _(u'О компании'),},
        {'app': 'news', 'label': _(u'Новости'),},
        {'app': 'organizers_app', 'label': _(u'Организаторам'),},
        {'app': 'press_app', 'label': _(u'Прессе'),},
        {'app': 'partners', 'label': _(u'Партнеры'),},
        {'label': _(u'План'), 'models': (
          {'label': _(u'Планы'), 'model': 'plans.plan'},
          {'label': _(u'Изображения для плана'), 'model': 'plans.planimage'},
          {'label': _(u'Иконки для плана'), 'model': 'plans.planicon'}
        )},
        {'app': 'publications', 'label': _(u'Сми о нас'),},

        {'label': _(u'Переводы'), 'icon':'icon-globe', 'url': '/rosetta/pick/'},
    ),
}

CKEDITOR_TOOLBAR = (
    [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templaates' ],
    [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','aRedo' ],
    [ 'Find','Replace','-','SelectAll','-','SpellChecker'],
    [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
    [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidaiRtl' ],
    [ 'Link','Unlink','Anchor' ],
    [ 'Image', 'Flash','Table','HorizontalRule','Smiley','SpecialChar','Modal' ],
    #[ 'Styles','Format','Font','FontaSize' ],
    #[ 'TextColor','BGColor' ],
    [ 'Maximize', 'ShowBlocks'],
)

CKEDITOR_CONFIGS = {
    'default': {
        #'extraPlugins': 'link',
        'toolbar': CKEDITOR_TOOLBAR,
        'height': 400,
        'width': 'auto',
        'disableNativeSpellChecker': False,
        'forcePasteAsPlainText': True,
        'startupOutlineBlocks': True,
    },
}

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
CKEDITOR_UPLOAD_PATH = 'filer_public/'
FILEBROWSER_DIRECTORY = CKEDITOR_UPLOAD_PATH

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "_static"),
 )

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

#rosetta
ROSETTA_MESSAGES_PER_PAGE = 30
ROSETTA_WSGI_AUTO_RELOAD = True
ROSETTA_UWSGI_AUTO_RELOAD = True
ROSETTA_MESSAGES_SOURCE_LANGUAGE_CODE = 'ru'
ROSETTA_MESSAGES_SOURCE_LANGUAGE_NAME = 'Russian'
ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'


#dynamic forms
USE_SITES = False
FORMS_BUILDER_HELPTEXT_MAX_LENGTH=4096

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_SERIALIZER_CLASS':
        'base.paginators.CustomPaginationSerializer',
    'DEFAULT_RENDERER_CLASSES':
        (
            'rest_framework.renderers.JSONRenderer',
        )
}

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = ['--with-notify',
             '--no-start-message',
             '--verbosity=2',
             '--with-fixture-bundling',
             ]

#mobile versions


try:
    from local_settings import *
except ImportError:
    pass
