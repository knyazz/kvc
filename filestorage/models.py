# coding: utf-8
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from easy_thumbnails.files import get_thumbnailer
from embed_video.fields import EmbedVideoField
from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from base.models import AbstractPage, TitlePage, ExpoType, Subject

from .choices import DOC_FILETYPES, MEDIA_FILETYPES


class Docfile(AbstractPage):
    docfile = models.FileField(_(u'Файл'), upload_to='docs/')
    file_type = models.CharField(_(u'Тип файла'), max_length=32,
                                choices=DOC_FILETYPES)
    flr_doc = FilerFileField(null=True, blank=True)

    class Meta:
        verbose_name=_(u'документ')
        verbose_name_plural = _(u'документы')


class Gallery(AbstractPage):
    order = models.PositiveIntegerField(_(u'Сортировка'))
    expotype = models.ForeignKey(ExpoType, null=True, blank=True,
                            verbose_name=ExpoType._meta.verbose_name) 
    subjects = models.ForeignKey(Subject, null=True, blank=True,
                                verbose_name=Subject._meta.verbose_name)
    event = models.ForeignKey('calendar_expo.Event', null=True, blank=True,
                                verbose_name=_(u'Мероприятие'))
    class Meta:
        ordering = 'order',
        verbose_name=_(u'Медиа банк')
        verbose_name_plural = _(u'Медиа банк')


class Mediafile(TitlePage):
    order = models.PositiveIntegerField(_(u'Сортировка'), default=0)
    fl_type = models.PositiveSmallIntegerField(_(u'Тип файла'), default=0,
                                                choices=MEDIA_FILETYPES)
    image = FilerImageField(verbose_name=_(u'Изображение'),
                            related_name='mediaimages',
                            null=True, blank=True)
    fl = FilerFileField(verbose_name=_(u'Файл (pdf)'), null=True, blank=True,)
    video = EmbedVideoField(blank=True,
                            help_text=_(u'например: http://www.youtube.ru/video'))
    gallery = models.ForeignKey(Gallery, null=True, blank=True,
                                verbose_name=Gallery._meta.verbose_name)

    class Meta:
        ordering = 'order', 'pk'
        verbose_name=_(u'Медиа файл')
        verbose_name_plural = _(u'Медиа файлы')

    def get_absolute_url(self):
        return reverse('filestorage:mediafile', kwargs=dict(pk=self.pk))

    is_image = property(lambda self: self.image and self.fl_type == 0)
    is_video = property(lambda self: self.video and self.fl_type == 1)
    is_file = property(lambda self: self.fl and self.fl_type == 2)

    @property
    def small_image(self):
        if self.is_image:
            opts = dict(size=(150,100), crop=True)
            try:
                thumbnail = get_thumbnailer(self.image).get_thumbnail(opts)
                return thumbnail.url
            except: pass