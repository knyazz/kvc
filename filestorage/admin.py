#coding: utf-8
from django.contrib import admin

from suit.admin import SortableModelAdmin, SortableStackedInline

from base.admin import FlatPageAdmin, AdminBaseMixin

#from .forms import MediafileForm
from .models import Docfile, Mediafile, Gallery


class DocfileAdmin(FlatPageAdmin):
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets = self.fieldsets[1:]
        self.fieldsets+= (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'docfile', 'file_type', 'flr_doc')
        }),)
admin.site.register(Docfile, DocfileAdmin)


class MediafileAdmin(SortableModelAdmin, AdminBaseMixin):
    sortable = 'order'
    class Media:
        js = (
                'js/admin.js',
                'js/jquery-ui-1.10.4.min.js'
        )
admin.site.register(Mediafile, MediafileAdmin)


class MediafilesInline(SortableStackedInline):
    model = Mediafile
    sortable = 'order'
    extra=0

class GalleryAdmin(SortableModelAdmin, FlatPageAdmin):
    inlines = (MediafilesInline,)
    sortable = 'order'
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fieldsets+= (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ( 'expotype', 'subjects', 'event')
        }),)

    class Media:
        js = (
                'js/admin-mediaflsets.js',
        )

admin.site.register(Gallery, GalleryAdmin)