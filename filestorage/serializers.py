from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
from embed_video.templatetags.embed_video_tags import VideoNode
from rest_framework import serializers
 
from .models import Gallery, Mediafile


class FilerImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilerImageField

class FilerFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilerFileField


class MediafileSerializer(serializers.ModelSerializer):
    image_url = serializers.Field(source="image.url")
    fl_url = serializers.Field(source="fl.url")
    fl_size = serializers.Field(source="fl.size")
    small_image = serializers.Field(source='small_image')
    is_video = serializers.Field(source='is_video')
    is_file = serializers.Field(source='is_file')
    class Meta:
        model = Mediafile
        fields = (  'id', 'video', 'image_url', 'small_image', 
                    'is_video', 'is_file', 'fl_url', 'fl_size',
        )

    def to_native(self, obj):
        data = super(MediafileSerializer, self).to_native(obj)
        data['title'] = obj.title(self.context.get('request'))
        if obj.is_video:
            data['render_video'] = VideoNode.embed(obj.video, size='small')
            data['video_thumbnail'] = VideoNode.get_backend(obj.video).thumbnail
        return data


class GallerySerializer(serializers.ModelSerializer):
    mediafile_set = MediafileSerializer()

    def to_native(self, obj):
        data = super(GallerySerializer, self).to_native(obj)
        data['title'] = obj.title(self.context.get('request'))
        return data

    class Meta:
        model = Gallery
        fields = 'id', 'mediafile_set'