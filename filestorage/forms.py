#coding: utf-8
from django import forms

from base.models import Subject, ExpoType
from base.forms import LocaleFieldModelFormMixin

from .models import Mediafile, Gallery


class MediafileForm(forms.ModelForm):
    class Meta:
        model = Mediafile


class EventForm(LocaleFieldModelFormMixin, forms.ModelForm):
    start_date = forms.DateField(required=False)
    end_date = forms.DateField(required=False)
    class Meta:
        model = Gallery

    def __init__(self, request=None, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['expotype'] = self.get_locale_field(ExpoType,request, 
                                                        multiple=False)
        self.fields['expotype'].empty_label = self.get_empty_label(request)
        self.fields['subjects'] = self.get_locale_field(Subject,request)