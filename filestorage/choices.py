#coding: utf-8
from django.utils.translation import ugettext_lazy as _


DOC_FILETYPES = (
    ('xls', 'XLS'),
    ('pdf', 'PDF'),
    ('doc', 'DOC'),
    ('docx', 'DOCX')
)

MEDIA_FILETYPES = (
    (0, _(u'Фото')),
    (1, _(u'Видео')),
    (2, _(u'Файл (pdf)')),
)