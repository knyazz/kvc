# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from base.tests import BaseTest


class FilestorageTest(BaseTest):
    def filestorage_simple_tests(self):
        '''
            test filestorage functionality
        '''
        # get documents page
        response = self.test_client.get(reverse('filestorage:documents'))
        self.assertEqual(response.status_code, 200)
        # get mediabank page
        response = self.test_client.get(reverse('filestorage:mediabank'))
        self.assertEqual(response.status_code, 200)
        # get mediabank_api_list
        response = self.test_client.get(reverse('filestorage:mediabank_api_list'))
        self.assertEqual(response.status_code, 200)
        # get mediafile page
        #kw = dict(pk=1)
        #response = self.test_client.get(reverse('filestorage:mediafile', kwargs=kw))
        #self.assertEqual(response.status_code, 200)