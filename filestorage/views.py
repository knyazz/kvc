#coding: utf-8
import datetime

from django.db.models import Q
from django.views.generic import DetailView, ListView

from rest_framework import generics

from base.models import GreenBlock
from base.views import ListPage, CTXMixin
from base.utils import format_locale_date

from .forms import EventForm
from .models import Docfile, Gallery, Mediafile, Subject
from .serializers import GallerySerializer


class DocumentList(ListPage):
    model=Docfile
    template_name='documents.html'

    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        ctx['greenblock'] = GreenBlock.objects.filter(page=3).last()
        return ctx
documents = DocumentList.as_view()


class MediabankMixin(CTXMixin):
    model=Gallery
    template_name='mediabank.html'
    paginate_by = 5
    def get_queryset(self):
        qs = super(MediabankMixin, self).get_queryset(
                                       ).active_pages(self.request)
        #TODO: refact this
        q = Q()
        if self.request.GET:
            body = self.request.GET
            if body.get('expotype'):
                q&= Q(expotype=body.get('expotype'))
            if body.getlist('subjects'):
                q&= Q(subjects__in=body.getlist('subjects'))
            if body.get('start_date'):
                date = format_locale_date(body.get('start_date'),self.request)
                q&= Q(event__end_date__gte=date)
            if body.get('end_date'):
                date = format_locale_date(body.get('end_date'),self.request)
                q&= Q(event__start_date__lte=date)
            if body.getlist('eventformat'):
                q&=Q(mediafile__fl_type__in=body.getlist('eventformat'))
        setq = set(qs.filter(q).values_list('pk', flat=True))
        return qs.filter(pk__in=setq)


class MediabankAPIList(MediabankMixin, generics.ListAPIView):
    serializer_class = GallerySerializer
mediabank_api_list = MediabankAPIList.as_view()

class MediabankList(MediabankMixin, ListView):
    u''' страница событий календаря'''
    def get_context_data(self, **kwargs):
        ctx = super(self.__class__, self).get_context_data(**kwargs)
        ctx['filt_eventformat'] = self.request.GET.getlist('eventformat')
        ctx['filt_subjects'] = self.request.GET.getlist('subject')
        ctx['subjects'] = Subject.objects.all()
        ctx['event_form'] = EventForm(self.request.GET)
        ctx['greenblock'] = GreenBlock.objects.filter(page=1).last()
        return ctx
mediabank = MediabankList.as_view()


class MediafileDetail(CTXMixin, DetailView):
    template_name = 'object_detail.html'
    context_object_name = 'detail_page'
mediafile = MediafileDetail.as_view( model=Mediafile)