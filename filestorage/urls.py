from django.conf.urls import patterns, url

urlpatterns = patterns('filestorage.views',
    url(r'^documents/$', 'documents', name='documents'),
    url(r'^mediabank/$', 'mediabank', name='mediabank'),
    url(r'^mediabank/api/list$', 'mediabank_api_list', name='mediabank_api_list'),
    url(r'^mediafile/(?P<pk>\d+)/$', 'mediafile', name='mediafile'),
)